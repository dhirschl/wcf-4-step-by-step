﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProductsEntityModel;
using System.Xml.Serialization;
using System.IO;
using System.Transactions;
using System.Security;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;

namespace ShoppingCartService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession,
        TransactionIsolationLevel=IsolationLevel.RepeatableRead)]
    public class ShoppingCartServiceImpl : IShoppingCartService
    {
        // The list of authorised users
        private string[] authorizedUsers = { "Fred@Adventure-Works.com",
                                             "Bert@Adventure-Works.com" };

        private List<ShoppingCartItem> shoppingCart =
            new List<ShoppingCartItem>();

        // Authorize the user if their email address is in the authorizedUsers list 
        private bool authorizeUser()
        {
            bool authorized = false;

            AuthorizationContext authContext =
                OperationContext.Current.ServiceSecurityContext.AuthorizationContext;
            foreach (ClaimSet claimSet in authContext.ClaimSets)
            {
                foreach (Claim emailClaim in
                    claimSet.FindClaims(ClaimTypes.Email, Rights.PossessProperty))
                {
                    foreach (string validUser in authorizedUsers)
                    {
                        if (String.Compare(emailClaim.Resource.ToString(), validUser, true) == 0)
                        {
                            authorized = true;
                            break;
                        }
                    }
                }
            }
            return authorized;
        }

        // Examine the shopping cart to determine whether an item with a  
        // specified product number has already been added. 
        // If so, return a reference to the item, otherwise return null 
        private ShoppingCartItem find(List<ShoppingCartItem> shoppingCart,
                                      string productNumber)
        {
            foreach (ShoppingCartItem item in shoppingCart)
            {
                if (string.Compare(item.ProductNumber, productNumber) == 0)
                {
                    return item;
                }
            }

            return null;
        }

        private bool decrementStockLevel(string productNumber)
        {
            // Decrement the current stock level of the selected product 
            // in the ProductInventory table.
            // If the update is successful then return true, otherwise return false.

            // The Product and ProductInventory tables are joined over the 
            // ProductID column.

            try
            {
                // Connect to the AdventureWorks database by using the Entity Framework
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Find the ProductID for the specified product
                    int productID =
                        (from p in database.Products
                         where String.Compare(p.ProductNumber, productNumber) == 0
                         select p.ProductID).First();

                    // Update the first row for this product in the ProductInventory table
                    // that has a quantity value greater than zero.
                    ProductInventory productInventory = database.ProductInventories.First(
                        pi => pi.ProductID == productID && pi.Quantity > 0);

                    // Update the stock level for the ProductInventory object
                    productInventory.Quantity --;

                    // Save the change back to the database
                    database.SaveChanges();
                }
            }
            catch
            {
                // If an exception occurs, return false to indicate failure
                return false;
            }

            // Return true to indicate success
            return true;
        }

        [OperationBehavior(TransactionScopeRequired=true,
                           TransactionAutoComplete=false)]
        public bool AddItemToCart(string productNumber)
        {
            // Check that the user is authorized.
            // Throw a securityException exception if not.
            if (!authorizeUser())
            {
                throw new SecurityException("Access denied");
            }

            try
            {
                // Check to see whether the user has already added this
                // product to the shopping cart
                ShoppingCartItem item = find(shoppingCart, productNumber);

                // If so, then simply increment the volume
                if (item != null)
                {
                    if (decrementStockLevel(productNumber))
                    {
                        item.Volume++;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                // Otherwise, retrieve the details of the product from the database
                else if (decrementStockLevel(productNumber))
                {
                    // Connect to the AdventureWorks database by using the Entity Framework
                    using (AdventureWorksEntities database = new AdventureWorksEntities())
                    {
                        // Retrieve the details of the selected product
                        Product product = (from p in database.Products
                                           where string.Compare(p.ProductNumber, productNumber) == 0
                                           select p).First();
                        
                        // Create and populate a new shopping cart item
                        ShoppingCartItem newItem = new ShoppingCartItem
                        {
                            ProductNumber = product.ProductNumber,
                            ProductName = product.Name,
                            Cost = product.ListPrice,
                            Volume = 1
                        };

                        // Add the new item to the shopping cart
                        shoppingCart.Add(newItem);

                        // Indicate success
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                // If an error occurs, finish and indicate failure
                return false;
            }
        }

        [OperationBehavior(TransactionScopeRequired = true,
                           TransactionAutoComplete = false)]
        public bool RemoveItemFromCart(string productNumber)
        {
            // Check that the user is authorized.
            // Throw a securityException exception if not.
            if (!authorizeUser())
            {
                throw new SecurityException("Access denied");
            }

            // Determine whether the specified product has an  
            // item in the shopping cart 
            ShoppingCartItem item = find(shoppingCart, productNumber);

            // If so, then decrement the volume 
            if (item != null)
            {
                item.Volume--;

                // If the volume is zero, remove the item from the shopping cart 
                if (item.Volume == 0)
                {
                    shoppingCart.Remove(item);
                }

                // Indicate success 
                return true;
            }

            // No such item in the shopping cart 
            return false;
        }

        [OperationBehavior(TransactionScopeRequired = true,
                           TransactionAutoComplete = false)]
        public string GetShoppingCart()
        {
            // Check that the user is authorized.
            // Throw a securityException exception if not.
            if (!authorizeUser())
            {
                throw new SecurityException("Access denied");
            }

            // Create a string holding a formatted representation  
            // of the shopping cart 
            string formattedContent = String.Empty;
            decimal totalCost = 0;

            foreach (ShoppingCartItem item in shoppingCart)
            {
                string itemString = String.Format(
                       "Number: {0}\tName: {1}\tCost: {2:C}\tVolume: {3}",
                       item.ProductNumber, item.ProductName, item.Cost,
                       item.Volume);
                totalCost += (item.Cost * item.Volume);
                formattedContent += itemString + "\n";
            }

            string totalCostString = String.Format("\nTotalCost: {0:C}", totalCost);
            formattedContent += totalCostString;
            return formattedContent;
        }

        [OperationBehavior(TransactionScopeRequired = true,
                           TransactionAutoComplete = false)]
        public bool Checkout()
        {
            // Check that the user is authorized.
            // Throw a securityException exception if not.
            if (!authorizeUser())
            {
                throw new SecurityException("Access denied");
            }

            // Not currently implemented
            // - just indicate that the transaction complete successfully
            // and return true
            OperationContext.Current.SetTransactionComplete();
            return true;
        }
    }
}
