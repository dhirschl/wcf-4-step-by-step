﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Xml.Serialization;

namespace ShoppingCartService
{
    // Shopping cart item
    [Serializable]
    public class ShoppingCartItem
    {
        public string ProductNumber { get; set; }
        public string ProductName { get; set; }
        public decimal Cost { get; set; }
        public int Volume { get; set; }
    }

    [ServiceContract(SessionMode = SessionMode.Required,
                     Namespace = "http://adventure-works.com/2010/06/04",
                     Name = "ShoppingCartService")]
    public interface IShoppingCartService
    {
        [OperationContract(Name = "AddItemToCart", IsInitiating = true)]
        [TransactionFlow(TransactionFlowOption.Mandatory)]
        bool AddItemToCart(string productNumber);

        [OperationContract(Name = "RemoveItemFromCart", IsInitiating = false)]
        [TransactionFlow(TransactionFlowOption.Mandatory)]
        bool RemoveItemFromCart(string productNumber);

        [OperationContract(Name = "GetShoppingCart", IsInitiating = false)]
        [TransactionFlow(TransactionFlowOption.Mandatory)]
        string GetShoppingCart();

        [OperationContract(Name = "Checkout", IsInitiating = false, IsTerminating = true)]
        [TransactionFlow(TransactionFlowOption.Mandatory)]
        bool Checkout();
    } 
}
