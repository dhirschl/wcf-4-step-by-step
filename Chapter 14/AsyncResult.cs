using System;
using System.Threading;

class AsyncResult<T> : IAsyncResult
{
    private T data;
    private object state;
    private bool isCompleted = false;
    private AutoResetEvent waitHandle;
    private bool isSynchronous = false;

    public T Data
    {
        set { data = value; }
        get { return data; }
    }

    public AsyncResult(bool synchronous, object stateData)
    {
        isSynchronous = synchronous;
        state = stateData;
    }

    public void Complete()
    {
        isCompleted = true;
        ((AutoResetEvent)AsyncWaitHandle).Set();
    }

    public object AsyncState
    {
        get { return state; }
    }

    public WaitHandle AsyncWaitHandle
    {
        get
        {
            if (waitHandle == null)
                waitHandle = new AutoResetEvent(false);

            return waitHandle;
        }
    }

    public bool CompletedSynchronously
    {
        get
        {
            if (!isCompleted)
                return false;
            else
                return isSynchronous;
        }
    }

    public bool IsCompleted
    {
        get { return isCompleted; }
    }
}