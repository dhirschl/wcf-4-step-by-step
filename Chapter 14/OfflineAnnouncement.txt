        // Asynchronously remove the specified service from the list registered with this discovery proxy
        protected override IAsyncResult OnBeginOfflineAnnouncement(DiscoveryMessageSequence messageSequence, EndpointDiscoveryMetadata endpointDiscoveryMetadata, AsyncCallback callback, object state)
        {
            Console.WriteLine("Starting OnBeginOfflineAnnouncement");

            // The logic in this method is very similar to that in OnBeginOnlineAnnoucement
            AsyncResult<object> result = new AsyncResult<object>(false, state);

            Task.Factory.StartNew(() =>
            {
                // Remove the service from the collection of registered servers
                this.RemoveService(endpointDiscoveryMetadata);
                result.Complete();

                Console.WriteLine("Calling back after removing service");
                if (callback != null)
                    callback(result);
            });

            Console.WriteLine("Returning after scheduling task to remove service");
            return result;
        }

        protected override void OnEndOfflineAnnouncement(IAsyncResult result)
        {
            Console.WriteLine("Starting OnEndOfflineAnnouncement");
            WaitForAsyncResult(result);
            Console.WriteLine("Leaving OnEndOfflineAnnouncement");
        }