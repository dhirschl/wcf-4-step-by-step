﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace ShoppingCartServiceRouter
{    
    public class Router : IRouter
    {
        private static IChannelFactory<IRequestChannel> factory = null;
        private EndpointAddress address1 = new EndpointAddress("http://localhost:9010/ShoppingCartService/ShoppingCartService.svc");
        private EndpointAddress address2 = new EndpointAddress("http://localhost:9020/ShoppingCartService/ShoppingCartService.svc");
        private static int routeBalancer = 1;

        static Router()
        {
            try
            {
                BasicHttpContextBinding service = new BasicHttpContextBinding();
                factory = service.BuildChannelFactory<IRequestChannel>();
                factory.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        public Message ProcessMessage(Message message)
        {
            IRequestChannel channel = null;

            Console.WriteLine("Action {0}", message.Headers.Action);
            try
            {
                if (routeBalancer % 2 == 0)
                {
                    channel = factory.CreateChannel(address1);
                    Console.WriteLine("Using {0}\n", address1.Uri);
                }
                else
                {
                    channel = factory.CreateChannel(address2);
                    Console.WriteLine("Using {0}\n", address2.Uri);
                }
                routeBalancer++;
                message.Properties.Remove("ContextMessageProperty");

                channel.Open();
                Message reply = channel.Request(message);
                channel.Close();
                return reply;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}
