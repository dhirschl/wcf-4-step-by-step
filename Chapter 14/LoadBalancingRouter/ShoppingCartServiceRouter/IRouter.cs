﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace ShoppingCartServiceRouter
{    
    [ServiceContract(Namespace="http://adventure-works.com/2010/15/07",Name="ShoppingCartServiceRouter")]
    public interface IRouter
    {
        [OperationContract(Action = "*", ReplyAction = "*")]
        Message ProcessMessage(Message message);
    }
}
