        // Add the specified service to the list of registered services
        private void AddService(EndpointDiscoveryMetadata metadata)
        {
            try
            {
                this.services.TryAdd(metadata.Address, metadata);

                foreach (var contractName in metadata.ContractTypeNames)
                {
                    Console.WriteLine("Added service with contract {0} at address {1}", contractName.ToString(), metadata.Address.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to add service at address {0}", metadata.Address.ToString());
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        // Remove the specified service from the list of registered services
        private void RemoveService(EndpointDiscoveryMetadata metadata)
        {
            try
            {
                EndpointDiscoveryMetadata data;
                this.services.TryRemove(metadata.Address, out data);

                Console.WriteLine("Removed service at address {0}", data.Address.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to remove service at address {0}", metadata.Address.ToString());
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }