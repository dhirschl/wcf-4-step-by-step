﻿using System;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;

namespace MessageInspector
{
    public class Inspector : IDispatchMessageInspector
    {
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            Console.WriteLine("Message received: {0}\n{1}\n\n", request.Headers.Action, request.Headers.To);
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            Console.WriteLine("Reply sent: {0}\n{1}\n\n", reply.Headers.Action, reply.Headers.To);
        }
    }

    public class InspectorBehavior : IServiceBehavior
    {
        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher chanDispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher epDispatcher in chanDispatcher.Endpoints)
                {
                    epDispatcher.DispatchRuntime.MessageInspectors.Add(new Inspector());
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
        }
    }

    public class ShoppingCartBehaviorExtensionElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get
            {
                return typeof(InspectorBehavior);
            }
        }

        protected override object CreateBehavior()
        {
            return new InspectorBehavior();
        }
    }
}
