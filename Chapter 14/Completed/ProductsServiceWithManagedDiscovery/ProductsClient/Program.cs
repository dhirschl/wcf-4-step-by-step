﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ProductsClient.ProductsService;
using System.ServiceModel.Discovery;
using System.ServiceModel.Description;
using System.Collections.Concurrent;

namespace ProductsClient
{
    class Program
    {
        private const string probeAddress = "net.tcp://localhost:8001/Probe";
        
        static void Main(string[] args)
        {            
            Console.WriteLine("Press ENTER when the Discovery Proxy has started");
            Console.ReadLine();

            // Creat a DiscoveryClient object that connects to the discovery proxy
            Uri discoveryProxyUri = new Uri(probeAddress);
            EndpointAddress discoveryProxyAddress = new EndpointAddress(discoveryProxyUri);
            DiscoveryEndpoint discoveryProxyEndpoint = 
                new DiscoveryEndpoint(new NetTcpBinding(), discoveryProxyAddress);
            DiscoveryClient discoveryClient = new DiscoveryClient(discoveryProxyEndpoint);
            
            // Find the announced endpoint for the Products Service
            FindCriteria productsServiceCriteria = new FindCriteria(typeof(IProductsService));
            FindResponse findResponse = discoveryClient.Find(productsServiceCriteria);
            EndpointAddress productsServiceAddress = findResponse.Endpoints[0].Address;

            // Connect to the Products Service
            ProductsServiceClient proxy = new ProductsServiceClient();
            proxy.Endpoint.Address = productsServiceAddress;

            // Test the operations in the service

            // Obtain a list of all products
            Console.WriteLine("Test 1: List all products");
            string[] productNumbers = proxy.ListProducts();
            foreach (string productNumber in productNumbers)
            {
                Console.WriteLine("Number: {0}", productNumber);
            }
            Console.WriteLine();

            // Fetch the details for a specific product
            Console.WriteLine("Test 2: Display the details for a product");
            ProductData product = proxy.GetProduct("WB-H098");
            Console.WriteLine("Number: {0}", product.ProductNumber);
            Console.WriteLine("Name: {0}", product.Name);
            Console.WriteLine("Color: {0}", product.Color);
            Console.WriteLine("Price: {0}", product.ListPrice);
            Console.WriteLine();

            // Query the stock level of this product
            Console.WriteLine("Test 3: Display the stock level of a product");
            int numInStock = proxy.CurrentStockLevel("WB-H098");
            Console.WriteLine("Current stock level: {0}", numInStock);
            Console.WriteLine();

            // Modify the stock level of this product
            Console.WriteLine("Test 4: Modify the stock level of a product");
            if (proxy.ChangeStockLevel("WB-H098", 100, "N/A", 0))
            {
                numInStock = proxy.CurrentStockLevel("WB-H098");
                Console.WriteLine("Stock changed. Current stock level: {0}", numInStock);
            }
            else
            {
                Console.WriteLine("Stock level update failed");
            }
            Console.WriteLine();

            // Disconnect from the service
            proxy.Close();
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
