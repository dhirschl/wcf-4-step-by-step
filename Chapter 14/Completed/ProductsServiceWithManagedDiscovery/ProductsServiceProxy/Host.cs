﻿using System;
using System.ServiceModel;
using System.ServiceModel.Discovery;

namespace ProductsServiceProxy
{
    class Program
    {
        private const string probeAddress = "net.tcp://localhost:8001/Probe";
        private const string announcementAddress = "net.tcp://localhost:8002/Announcement";

        static void Main(string[] args)
        {
            // Create service host for discovery proxy
            ServiceHost proxyService = new ServiceHost(new ProductsServiceProxy());

            // Create probe endpoint for discovery proxy
            DiscoveryEndpoint discoveryEndpoint = new DiscoveryEndpoint();
            discoveryEndpoint.Binding = new NetTcpBinding();
            discoveryEndpoint.Address = new EndpointAddress(probeAddress);
            discoveryEndpoint.IsSystemEndpoint = false;

            // Create announcement endpoint for discovery proxy
            AnnouncementEndpoint announcementEndpoint = new AnnouncementEndpoint();
            announcementEndpoint.Binding = new NetTcpBinding();
            announcementEndpoint.Address = new EndpointAddress(announcementAddress);

            // Add endpoints to the service host
            proxyService.AddServiceEndpoint(discoveryEndpoint);
            proxyService.AddServiceEndpoint(announcementEndpoint);            

            // Start the service
            proxyService.Open();
            Console.WriteLine("Discovery Proxy Service running\n");

            Console.WriteLine("Press ENTER to stop");
            Console.ReadLine();

            // Stop the service and finish
            proxyService.Close();
        }
    }
}
