﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Discovery;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;

namespace ProductsServiceProxy
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, 
                     ConcurrencyMode = ConcurrencyMode.Multiple)]
    class ProductsServiceProxy : DiscoveryProxy
    {
        // Store data for registered services in a ConcurrentDictionary object
        ConcurrentDictionary<EndpointAddress, EndpointDiscoveryMetadata> services;

        public ProductsServiceProxy()
        {
            this.services = new ConcurrentDictionary<EndpointAddress, EndpointDiscoveryMetadata>();
        }

        // Add the specified service to the list of registered services
        private void AddService(EndpointDiscoveryMetadata metadata)
        {
            try
            {
                this.services.TryAdd(metadata.Address, metadata);

                foreach (var contractName in metadata.ContractTypeNames)
                {
                    Console.WriteLine("Added service with contract {0} at address {1}", contractName.ToString(), metadata.Address.ToString());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to add service at address {0}", metadata.Address.ToString());
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        // Remove the specified service from the list of registered services
        private void RemoveService(EndpointDiscoveryMetadata metadata)
        {
            try
            {
                EndpointDiscoveryMetadata data;
                this.services.TryRemove(metadata.Address, out data);

                Console.WriteLine("Removed service at address {0}", data.Address.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to remove service at address {0}", metadata.Address.ToString());
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        // Search through the list of registered services to find all matching service
        private void FindService(FindRequestContext requestContext)
        {            
            try
            {
                // Find all services that match the criteria specified by the request context
                var matches = from service in this.services
                              where requestContext.Criteria.IsMatch(service.Value)
                              select service;

                // Iterate through the list of services and add them 
                // to the list of services in the FindRequestContext parameter
                foreach (var data in matches)
                {
                    Console.WriteLine("Found matching service endpoint at {0}", data.Value.Address);
                    requestContext.AddMatchingEndpoint(data.Value);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to find service with criteria {0}", 
                    requestContext.Criteria.ToString());
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }

        // Search through the list of registered services to find a service 
        // with and address that matches that in the specified ResolveCriteria
        private EndpointDiscoveryMetadata ResolveService(ResolveCriteria criteria)
        {
            try
            {
                // Find the first service that matches the specified address
                var match = (from service in this.services
                             where service.Value.Address == criteria.Address
                             select service).First();

                Console.WriteLine("Resolved service endpoint at {0}", match.Value.Address);

                // Return the service
                return match.Value;
            }
            // If there is no matching service, the LINQ query throws an exception
            // In this case, return null
            catch (Exception e)
            {
                Console.WriteLine("Failed to resolve service with address {0}", criteria.Address);
                Console.WriteLine("Exception: {0}", e.Message);
                return null;
            }
        }

        private void WaitForAsyncResult(IAsyncResult result)
        {
            // The IAsyncResult parameter should be an AsyncResult object returned by the OnBegin method.
            // If it is some other type, then this method will return without waiting
            AsyncResult<object> r = result as AsyncResult<object>;

            // If the OnBeginOnlineAnnouncement did not complete synchronously 
            // then wait until the AsyncWaitHandle property says that the async operation is complete
            if ((r != null) && !r.CompletedSynchronously)
            {
                WaitHandle waitHandle = r.AsyncWaitHandle;
                waitHandle.WaitOne();
            }
        }

        // Asynchronously add the specified service to the list registered with this discovery proxy
        protected override IAsyncResult OnBeginOnlineAnnouncement(DiscoveryMessageSequence messageSequence, 
            EndpointDiscoveryMetadata endpointDiscoveryMetadata, AsyncCallback callback, object state)
        {
            Console.WriteLine("Starting OnBeginOnlineAnnouncement");

            // Create an AsyncResult object to pass back for synchronization purposes
            AsyncResult<object> result = new AsyncResult<object>(false, state);

            // Use a Task to add the service in the background
            Task.Factory.StartNew(() =>
            {
                // Add the service to the collection of registered services
                this.AddService(endpointDiscoveryMetadata);

                // Indicate that the operation is complete
                result.Complete();

                // Invoke callback and pass the AsyncResult object as the parameter
                Console.WriteLine("Calling back after adding service");
                if (callback != null)
                    callback(result);
            });

            // Return the AsyncResult object
            Console.WriteLine("Returning after scheduling task to add service");
            return result;
        }

        protected override void OnEndOnlineAnnouncement(IAsyncResult result)
        {
            Console.WriteLine("Starting OnEndOnlineAnnouncement");
            WaitForAsyncResult(result);
            Console.WriteLine("Leaving OnEndOnlineAnnouncement");
        }

        // Asynchronously remove the specified service from the list registered with this discovery proxy
        protected override IAsyncResult OnBeginOfflineAnnouncement(DiscoveryMessageSequence messageSequence, EndpointDiscoveryMetadata endpointDiscoveryMetadata, AsyncCallback callback, object state)
        {
            Console.WriteLine("Starting OnBeginOfflineAnnouncement");

            // The logic in this method is very similar to that in OnBeginOnlineAnnoucement
            AsyncResult<object> result = new AsyncResult<object>(false, state);

            Task.Factory.StartNew(() =>
            {
                // Remove the service from the collection of registered servers
                this.RemoveService(endpointDiscoveryMetadata);
                result.Complete();

                Console.WriteLine("Calling back after removing service");
                if (callback != null)
                    callback(result);
            });

            Console.WriteLine("Returning after scheduling task to remove service");
            return result;
        }

        protected override void OnEndOfflineAnnouncement(IAsyncResult result)
        {
            Console.WriteLine("Starting OnEndOfflineAnnouncement");
            WaitForAsyncResult(result);
            Console.WriteLine("Leaving OnEndOfflineAnnouncement");
        }

        protected override IAsyncResult OnBeginFind(FindRequestContext findRequestContext, AsyncCallback callback, object state)
        {
            Console.WriteLine("Starting OnBeginFind");
            AsyncResult<FindRequestContext> result = new AsyncResult<FindRequestContext>(false, state);

            Task.Factory.StartNew(() =>
            {
                this.FindService(findRequestContext);
                result.Complete();

                Console.WriteLine("Calling back after finding service");
                if (callback != null)
                    callback(result);
            });

            Console.WriteLine("Returning after scheduling task to find service");
            return result;
        }

        protected override void OnEndFind(IAsyncResult result)
        {
            Console.WriteLine("Starting OnEndFind");
            WaitForAsyncResult(result);
            Console.WriteLine("Leaving OnEndFind");
        }

        protected override IAsyncResult OnBeginResolve(ResolveCriteria resolveCriteria, AsyncCallback callback, object state)
        {
            Console.WriteLine("Starting OnBeginResolve");
            AsyncResult<EndpointDiscoveryMetadata> result = new AsyncResult<EndpointDiscoveryMetadata>(false, state);
            result.Data = null;

            Task.Factory.StartNew(() =>
            {
                EndpointDiscoveryMetadata data = this.ResolveService(resolveCriteria);
                result.Data = data;
                result.Complete();

                Console.WriteLine("Calling back after resolving service");
                if (callback != null)
                    callback(result);
            });

            Console.WriteLine("Returning after scheduling task to resolve service");
            return result;
        }

        protected override EndpointDiscoveryMetadata OnEndResolve(IAsyncResult result)
        {
            Console.WriteLine("Starting OnEndResolve");
            WaitForAsyncResult(result);
            if (result is AsyncResult<EndpointDiscoveryMetadata>)
            {
                Console.WriteLine("Returning result from OnEndResolve");
                return ((AsyncResult<EndpointDiscoveryMetadata>)result).Data;
            }
            else
            {
                Console.WriteLine("Returning null from OnEndResolve");
                return null;
            }
        }
    }
}
