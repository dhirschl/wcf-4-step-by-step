﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ProductsClient.ProductsService;
using System.ServiceModel.Discovery;
using System.ServiceModel.Description;
using System.Collections.Concurrent;

namespace ProductsClient
{
    class Program
    {
        // Store data for announced services in a ConcurrentDictionary object
        private static ConcurrentDictionary<EndpointAddress, EndpointDiscoveryMetadata> services = 
            new ConcurrentDictionary<EndpointAddress, EndpointDiscoveryMetadata>();
        
        static void Main(string[] args)
        {
            // Use Service Announcements events to track the location and status of services
            AnnouncementService announcementService = new AnnouncementService();

            announcementService.OnlineAnnouncementReceived += (sender, eventArgs) =>
            {
                Console.WriteLine("Online announcement received");
                try
                {
                    services.TryAdd(eventArgs.EndpointDiscoveryMetadata.Address, 
                        eventArgs.EndpointDiscoveryMetadata);

                    foreach (var contractName in eventArgs.EndpointDiscoveryMetadata.ContractTypeNames)
                    {
                        Console.WriteLine("Added service with contract {0} at address {1}", 
                            contractName.ToString(), 
                            eventArgs.EndpointDiscoveryMetadata.Address.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to add service at address {0}", 
                        eventArgs.EndpointDiscoveryMetadata.Address.ToString());
                    Console.WriteLine("Exception: {0}", e.Message);
                }
            };

            announcementService.OfflineAnnouncementReceived += (sender, eventArgs) =>
            {
                try
                {
                    Console.WriteLine("Offline announcement received");
                    EndpointDiscoveryMetadata data;
                    services.TryRemove(eventArgs.EndpointDiscoveryMetadata.Address, out data);

                    Console.WriteLine("Removed service at address {0}", data.Address.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to remove service at address {0}", eventArgs.EndpointDiscoveryMetadata.Address.ToString());
                    Console.WriteLine("Exception: {0}", e.Message);
                }
            };

            ServiceHost announcementServiceHost = new ServiceHost(announcementService);
            announcementServiceHost.AddServiceEndpoint(new UdpAnnouncementEndpoint());
            announcementServiceHost.Open();

            Console.WriteLine("Client listening for announcements");
            Console.WriteLine("Press ENTER when the Products Service is available");
            Console.ReadLine();

            // Find the announced endpoint for the Products Service
            FindCriteria productsServiceCriteria = new FindCriteria(typeof(IProductsService));
            EndpointAddress productsServiceAddress = 
                (from service in services
                 where productsServiceCriteria.IsMatch(service.Value)
                 select service.Key).First();

            // Connect to the Products Service
            ProductsServiceClient proxy = new ProductsServiceClient();
            proxy.Endpoint.Address = productsServiceAddress;

            // Test the operations in the service

            // Obtain a list of all products
            Console.WriteLine("Test 1: List all products");
            string[] productNumbers = proxy.ListProducts();
            foreach (string productNumber in productNumbers)
            {
                Console.WriteLine("Number: {0}", productNumber);
            }
            Console.WriteLine();

            // Fetch the details for a specific product
            Console.WriteLine("Test 2: Display the details for a product");
            ProductData product = proxy.GetProduct("WB-H098");
            Console.WriteLine("Number: {0}", product.ProductNumber);
            Console.WriteLine("Name: {0}", product.Name);
            Console.WriteLine("Color: {0}", product.Color);
            Console.WriteLine("Price: {0}", product.ListPrice);
            Console.WriteLine();

            // Query the stock level of this product
            Console.WriteLine("Test 3: Display the stock level of a product");
            int numInStock = proxy.CurrentStockLevel("WB-H098");
            Console.WriteLine("Current stock level: {0}", numInStock);
            Console.WriteLine();

            // Modify the stock level of this product
            Console.WriteLine("Test 4: Modify the stock level of a product");
            if (proxy.ChangeStockLevel("WB-H098", 100, "N/A", 0))
            {
                numInStock = proxy.CurrentStockLevel("WB-H098");
                Console.WriteLine("Stock changed. Current stock level: {0}", numInStock);
            }
            else
            {
                Console.WriteLine("Stock level update failed");
            }
            Console.WriteLine();

            // Disconnect from the service
            proxy.Close();
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
