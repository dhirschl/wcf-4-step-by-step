﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProductsEntityModel;
using System.ServiceModel.Description;

namespace ShoppingCartService
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.PerSession)]
    [Serializable]
    [DurableService]
    public class ShoppingCartServiceImpl : IShoppingCartService
    {
        private List<ShoppingCartItem> shoppingCart =
            new List<ShoppingCartItem>();

        // Examine the shopping cart to determine whether an item with a  
        // specified product number has already been added. 
        // If so, return a reference to the item, otherwise return null 
        private ShoppingCartItem find(List<ShoppingCartItem> shoppingCart,
                                      string productNumber)
        {
            foreach (ShoppingCartItem item in shoppingCart)
            {
                if (string.Compare(item.ProductNumber, productNumber) == 0)
                {
                    return item;
                }
            }

            return null;
        }

        [DurableOperation(CanCreateInstance = true)]
        public bool AddItemToCart(string productNumber)
        {
            // Note: For clarity, this method performs very limited
            // security checking and exception handling
            try
            {
                // Check to see whether the user has already added this
                // product to the shopping cart
                ShoppingCartItem item = find(shoppingCart, productNumber);

                // If so, then simply increment the volume
                if (item != null)
                {
                    item.Volume++;
                    return true;
                }

                // Otherwise, retrieve the details of the product from the database
                else
                {
                    // Connect to the AdventureWorks database by using the Entity Framework
                    using (AdventureWorksEntities database = new AdventureWorksEntities())
                    {
                        // Retrieve the details of the selected product
                        Product product = (from p in database.Products
                                           where string.Compare(p.ProductNumber, productNumber) == 0
                                           select p).First();
                        
                        // Create and populate a new shopping cart item
                        ShoppingCartItem newItem = new ShoppingCartItem
                        {
                            ProductNumber = product.ProductNumber,
                            ProductName = product.Name,
                            Cost = product.ListPrice,
                            Volume = 1
                        };

                        // Add the new item to the shopping cart
                        shoppingCart.Add(newItem);

                        // Indicate success
                        return true;
                    }
                }
            }
            catch
            {
                // If an error occurs, finish and indicate failure
                return false;
            }
        }

        [DurableOperation(CanCreateInstance = false)]
        public bool RemoveItemFromCart(string productNumber)
        {
            // Determine whether the specified product has an  
            // item in the shopping cart 
            ShoppingCartItem item = find(shoppingCart, productNumber);

            // If so, then decrement the volume 
            if (item != null)
            {
                item.Volume--;

                // If the volume is zero, remove the item from the shopping cart 
                if (item.Volume == 0)
                {
                    shoppingCart.Remove(item);
                }

                // Indicate success 
                return true;
            }

            // No such item in the shopping cart 
            return false;
        }

        [DurableOperation(CanCreateInstance = false)]
        public string GetShoppingCart()
        {
            // Create a string holding a formatted representation  
            // of the shopping cart 
            string formattedContent = String.Empty;
            decimal totalCost = 0;

            foreach (ShoppingCartItem item in shoppingCart)
            {
                string itemString = String.Format(
                       "Number: {0}\tName: {1}\tCost: {2:C}\tVolume: {3}",
                       item.ProductNumber, item.ProductName, item.Cost,
                       item.Volume);
                totalCost += (item.Cost * item.Volume);
                formattedContent += itemString + "\n";
            }

            string totalCostString = String.Format("\nTotalCost: {0:C}", totalCost);
            formattedContent += totalCostString;
            return formattedContent;
        }

        [DurableOperation(CanCreateInstance = false, CompletesInstance = true)]
        public bool Checkout()
        {
            // Not currently implemented - just return true
            return true;
        }
    }
}
