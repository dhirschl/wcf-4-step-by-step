﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Routing;

namespace StaticRouter
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                ServiceHost routerHost = new ServiceHost(typeof(RoutingService));
                routerHost.Open();
                Console.WriteLine("Router running");
                Console.WriteLine("Press ENTER to stop the service");
                Console.ReadLine();
                routerHost.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
