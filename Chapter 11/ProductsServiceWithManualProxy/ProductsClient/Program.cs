﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Security;

namespace ProductsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();

            try
            {
                WS2007HttpBinding httpBinding = new WS2007HttpBinding(SecurityMode.Message);
                WSHttpSecurity httpSec = httpBinding.Security;
                httpSec.Message.AlgorithmSuite = SecurityAlgorithmSuite.Basic128;
                httpSec.Message.ClientCredentialType = MessageCredentialType.Windows;

                EndpointAddress address = new EndpointAddress(
                    "http://localhost:8010/ProductsService/Service.svc");

                ProductsServiceProxy channel = new ProductsServiceProxy(httpBinding, address);
                channel.ClientCredentials.Windows.ClientCredential.UserName = "Fred";
                channel.ClientCredentials.Windows.ClientCredential.Password = "Pa$$w0rd";
                channel.ClientCredentials.Windows.ClientCredential.Domain = "LON-DEV-01";

                Console.WriteLine("Test 1: List all bicycle frames"); 
                List<string> productNumbers = channel.ListMatchingProducts("Frame"); 
                foreach (string productNumber in productNumbers) 
                { 
                    Console.WriteLine("Number: {0}", productNumber); 
                } 
                Console.WriteLine();

                channel.Close();
                channel = null;
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
