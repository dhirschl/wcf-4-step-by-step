﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ProductsClient
{
    class ProductsServiceProxy : ClientBase<Products.IProductsServiceV2>,
                                 Products.IProductsServiceV2
    {
        public ProductsServiceProxy(Binding binding, EndpointAddress address) :
            base(binding, address)
        {
        }

        public List<string> ListMatchingProducts(string match)
        {
            return base.Channel.ListMatchingProducts(match);
        }

        public Products.ProductData GetProduct(string productNumber)
        {
            return base.Channel.GetProduct(productNumber);
        }

        public int CurrentStockLevel(string productNumber)
        {
            return base.Channel.CurrentStockLevel(productNumber);
        }

        public bool ChangeStockLevel(string productNumber, short newStockLevel, string shelf, int bin)
        {
            return base.Channel.ChangeStockLevel(productNumber, newStockLevel, shelf, bin);
        }
    }
}
