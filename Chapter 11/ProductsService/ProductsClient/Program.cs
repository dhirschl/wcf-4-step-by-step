﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Security;

namespace ProductsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();

            try
            {
                // Create the HTTP binding and configure security
                WS2007HttpBinding httpBinding = new WS2007HttpBinding(SecurityMode.Message);
                WSHttpSecurity httpSec = httpBinding.Security;
                httpSec.Message.AlgorithmSuite = SecurityAlgorithmSuite.Basic128;
                httpSec.Message.ClientCredentialType = MessageCredentialType.Windows;

                // Create an endpoint to connect to the service
                EndpointAddress address = new EndpointAddress("http://localhost:8010/ProductsService/Service.svc");

                // Build the channel stack for communicating with the service
                Products.IProductsServiceV2 channel = ChannelFactory<Products.IProductsServiceV2>.CreateChannel(httpBinding, address);

                // Obtain the list of bicycle frames
                Console.WriteLine("Test 1: List all bicycle frames");
                List<string> productNumbers = channel.ListMatchingProducts("Frame");
                foreach (string productNumber in productNumbers)
                {
                    Console.WriteLine("Number: {0}", productNumber);
                }
                Console.WriteLine();

                // Close the connection to the service
                channel = null;
            }
             catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
