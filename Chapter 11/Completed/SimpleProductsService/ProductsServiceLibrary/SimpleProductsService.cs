﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProductsEntityModel;
using System.Security.Permissions;

namespace Products
{
    // WCF Service that implements the service contract
    // This implementation performs minimal error checking and exception handling
    public class SimpleProductsServiceImpl : ISimpleProductsService
    {
        public List<string> ListProducts()
        {
            // Create a list for holding product numbers
            List<string> productsList = new List<string>();

            try
            {
                // Connect to the AdventureWorks database by using the Entity Framework
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    /// Fetch the product number of every product in the database
                    var products = from product in database.Products
                                   select product.ProductNumber;

                    productsList = products.ToList();
                }
            }
            catch (Exception e)
            {
                // Ignore any errors
            }

            // Return the list of product numbers
            return productsList;
        }        
    }   
}