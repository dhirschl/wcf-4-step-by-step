﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Security;


namespace Products
{
    // Service contract describing the operations provided by the WCF service
    [ServiceContract(Namespace="http://adventure-works.com/2010/06/29", 
                     Name="SimpleProductsService")]
    public interface ISimpleProductsService
    {
        [OperationContract(Name="ListProducts")]
        List<string> ListProducts();
    }
}