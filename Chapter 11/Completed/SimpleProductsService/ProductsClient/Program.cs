﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ProductsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();

            try
            {
                // Create a default HTTP binding
                BasicHttpBinding httpBinding = new BasicHttpBinding();

                // Create an endpoint
                EndpointAddress address = new EndpointAddress(
                    "http://localhost:8010/SimpleProductsService/Service.svc");

                IChannelFactory<IRequestChannel> factory =
                    httpBinding.BuildChannelFactory<IRequestChannel>();
                factory.Open();

                IRequestChannel channel = factory.CreateChannel(address);
                channel.Open();

                Message request = Message.CreateMessage(MessageVersion.Soap11,
                    "http://adventure-works.com/2010/06/29/SimpleProductsService/ListProducts");

                Message reply = channel.Request(request);
                Console.WriteLine(reply);

                request.Close();
                reply.Close();
                channel.Close();
                factory.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
