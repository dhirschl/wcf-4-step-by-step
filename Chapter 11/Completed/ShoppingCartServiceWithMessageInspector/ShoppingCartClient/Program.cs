﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ShoppingCartClient.ShoppingCartService;
using System.Transactions;

namespace ShoppingCartClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();
            try
            {
                // Connect to the ShoppingCartService service 
                ShoppingCartServiceClient proxy =
                    new ShoppingCartServiceClient("CustomBinding_IShoppingCartService");

                TransactionOptions tOpts = new TransactionOptions();
                tOpts.IsolationLevel = IsolationLevel.RepeatableRead;
                tOpts.Timeout = new TimeSpan(0, 1, 0);
                using (TransactionScope tx =
                    new TransactionScope(TransactionScopeOption.RequiresNew, tOpts))
                {

                    // Add two water bottles to the shopping cart 
                    proxy.AddItemToCart("WB-H098");
                    proxy.AddItemToCart("WB-H098");

                    // Add a mountain seat assembly to the shopping cart 
                    proxy.AddItemToCart("SA-M198");

                    // Query the shopping cart and display the result 
                    string cartContents = proxy.GetShoppingCart();
                    Console.WriteLine(cartContents);

                    // Buy the goods in the shopping cart 
                    if (proxy.Checkout())
                    {
                        tx.Complete();
                        Console.WriteLine("Goods purchased");
                    }
                }

                // Disconnect from the ShoppingCartService service 
                proxy.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
