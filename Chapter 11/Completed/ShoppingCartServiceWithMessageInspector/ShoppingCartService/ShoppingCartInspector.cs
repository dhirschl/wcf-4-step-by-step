﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;
using System.ServiceModel.Configuration;

namespace ShoppingCartService
{
    class ShoppingCartInspector : IDispatchMessageInspector
    {
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            Console.WriteLine("Message received: {0}\n{1}\n\n",
                request.Headers.Action, request.ToString());
            return null;
        }

        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            Console.WriteLine("Reply sent: {0}\n{1}\n\n",
                reply.Headers.Action, reply.ToString());
        }
    }

    public class ShoppingCartBehavior : IServiceBehavior
    {
        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {            
        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcher chanDispatcher in serviceHostBase.ChannelDispatchers)
            {
                foreach (EndpointDispatcher epDispatcher in chanDispatcher.Endpoints)
                {
                    epDispatcher.DispatchRuntime.MessageInspectors.Add(new ShoppingCartInspector());
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {            
        }
    }

    public class ShoppingCartBehaviorExtensionElement : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get 
            { 
                return typeof(ShoppingCartBehavior); 
            }
        }

        protected override object CreateBehavior()
        {
            return new ShoppingCartBehavior();
        }
    }
}
