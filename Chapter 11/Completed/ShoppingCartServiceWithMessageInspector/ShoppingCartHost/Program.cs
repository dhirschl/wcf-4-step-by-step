﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace ShoppingCartHost
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomBinding customBinding = new CustomBinding();

            TransactionFlowBindingElement txFlowBindElement = new TransactionFlowBindingElement();
            txFlowBindElement.TransactionProtocol = TransactionProtocol.OleTransactions;
            customBinding.Elements.Add(txFlowBindElement);

            ReliableSessionBindingElement rsBindElement = new ReliableSessionBindingElement();
            rsBindElement.FlowControlEnabled = true;
            rsBindElement.Ordered = true;
            customBinding.Elements.Add(rsBindElement);

            SecurityBindingElement secBindElement =
                SecurityBindingElement.CreateSecureConversationBindingElement(
                    SecurityBindingElement.CreateSspiNegotiationBindingElement());
            secBindElement.LocalServiceSettings.DetectReplays = true;
            customBinding.Elements.Add(secBindElement);

            customBinding.Elements.Add(new TextMessageEncodingBindingElement());
            TcpTransportBindingElement tcpBindElement = new TcpTransportBindingElement();
            tcpBindElement.TransferMode = TransferMode.Buffered;
            customBinding.Elements.Add(tcpBindElement);

            ServiceHost host = new ServiceHost(typeof(ShoppingCartService.ShoppingCartServiceImpl));

            host.AddServiceEndpoint(typeof(ShoppingCartService.IShoppingCartService),
                customBinding, "net.tcp://localhost:8090/ShoppingCartService");
            //host.Description.Behaviors.Add(new ShoppingCartService.ShoppingCartBehavior());
            host.Open();
            Console.WriteLine("Service running");
            Console.WriteLine("Press ENTER to stop the service");
            Console.ReadLine();
        }
    }
}
