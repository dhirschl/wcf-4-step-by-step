﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Statements;

namespace ProductsWorkflowClient
{

    class Program
    {
        static void Main(string[] args)
        {            
            WorkflowInvoker.Invoke(new ClientWorkflow());
        }
    }
}
