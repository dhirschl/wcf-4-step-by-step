﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Statements;
using System.ServiceModel.Activities;
using System.Xaml;

namespace ShoppingCartHost
{

    class Program
    {
        static void Main(string[] args)
        {
            WorkflowService service =
                XamlServices.Load(@"..\..\..\ShoppingCartService\ShoppingCartService.xamlx")
                as WorkflowService;

            WorkflowServiceHost host = new WorkflowServiceHost(service);
            host.Open();
            Console.WriteLine("Service running. Press ENTER to stop");
            Console.ReadLine();
            host.Close();
        }
    }
}
