﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Runtime.Serialization;
using System.ServiceModel;
using ProductsEntityModel;

namespace ProductsWorkflowService
{
    // Classes for passing fault information back to client applications
    [DataContract]
    public class SystemFault
    {
        [DataMember]
        public string SystemOperation { get; set; }

        [DataMember]
        public string SystemReason { get; set; }

        [DataMember]
        public string SystemMessage { get; set; }
    }

    [DataContract]
    public class DatabaseFault
    {
        [DataMember]
        public string DbOperation { get; set; }

        [DataMember]
        public string DbReason { get; set; }

        [DataMember]
        public string DbMessage { get; set; }
    }

    // Data contract describing the details of a product passed to client applications
    [DataContract]
    public class ProductData
    {
        [DataMember]
        public string Name;

        [DataMember]
        public string ProductNumber;

        [DataMember]
        public string Color;

        [DataMember]
        public decimal ListPrice;
    }

    public sealed class ProductExists : CodeActivity<Boolean>
    {
        public InArgument<AdventureWorksEntities> Database { get; set; }
        public InArgument<string> ProductNumber { get; set; }

        protected override bool Execute(CodeActivityContext context)
        {
            // Retrieve the product number and database reference from the input arguments
            string productNumber = ProductNumber.Get(context);
            AdventureWorksEntities database = Database.Get(context);

            // Check to see whether the specified product exists in the database
            int numProducts = (from p in database.Products
                               where string.Equals(p.ProductNumber, productNumber)
                               select p).Count();

            return numProducts > 0;
        }
    }

    public sealed class FindProduct : CodeActivity<ProductData>
    {
        public InArgument<AdventureWorksEntities> Database { get; set; }
        public InArgument<string> ProductNumber { get; set; }

        protected override ProductData Execute(CodeActivityContext context)
        {
            // Retrieve the product number and database reference from the input arguments
            string productNumber = ProductNumber.Get(context);
            AdventureWorksEntities database = Database.Get(context);

            // Find the first product that matches the specified product number
            Product matchingProduct = database.Products.First(
                p => String.Compare(p.ProductNumber, productNumber) == 0);

            ProductData productData = new ProductData()
            {
                Name = matchingProduct.Name,
                ProductNumber = matchingProduct.ProductNumber,
                Color = matchingProduct.Color,
                ListPrice = matchingProduct.ListPrice
            };

            return productData;
        }
    }
}
