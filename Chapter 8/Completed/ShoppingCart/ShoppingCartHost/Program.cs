﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.DurableInstancing;
using System.Activities.Statements;
using System.ServiceModel.Activities;
using System.Xaml;

namespace ShoppingCartHost
{

    class Program
    {
        static void Main(string[] args)
        {
            WorkflowService service = XamlServices.Load(@"..\..\..\ShoppingCartService\ShoppingCartService.xamlx") as WorkflowService;

            WorkflowServiceHost host = new WorkflowServiceHost(service);

            string persistenceStoreConnectionString =
                @"Data Source=.;Initial Catalog=WCFPersistence;Integrated Security=True";
            SqlWorkflowInstanceStore instanceStore = new SqlWorkflowInstanceStore(persistenceStoreConnectionString);
            host.DurableInstancingOptions.InstanceStore = instanceStore;

            host.Open();
            Console.WriteLine("Service running. Press ENTER to stop");
            Console.ReadLine();
            host.Close();
        }
    }
}
