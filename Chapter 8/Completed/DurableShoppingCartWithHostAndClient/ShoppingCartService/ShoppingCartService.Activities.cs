﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using ProductsEntityModel;

namespace ShoppingCartService
{
    // Shopping cart item
    public class ShoppingCartItem
    {
        public string ProductNumber { get; set; }
        public string ProductName { get; set; }
        public decimal Cost { get; set; }
        public int Volume { get; set; }
    }

    public sealed class FindItem : CodeActivity<ShoppingCartItem>
    {
        public InArgument<List<ShoppingCartItem>> ShoppingCart { get; set; }
        public InArgument<string> ProductNumber { get; set; }

        // Examine the shopping cart to determine whether an item with a
        // specified product number has already been added.
        // If so, return a reference to the item, otherwise return null.
        protected override ShoppingCartItem Execute(CodeActivityContext context)
        {
            string productNumber = ProductNumber.Get(context);
            foreach (ShoppingCartItem item in context.GetValue(this.ShoppingCart))
            {
                if (string.Compare(item.ProductNumber, productNumber) == 0)
                {
                    return item;
                }
            }

            return null;
        }
    }

    public sealed class GetItemFromDatabase : CodeActivity<Product>
    {
        public InArgument<AdventureWorksEntities> Database { get; set; }
        public InArgument<string> ProductNumber { get; set; }

        // Retrieve the specified product from the database
        protected override Product Execute(CodeActivityContext context)
        {
            try
            {
                string productNumber = ProductNumber.Get(context);
                AdventureWorksEntities database = Database.Get(context);

                Product product = (from p in database.Products
                                   where string.Compare(p.ProductNumber, productNumber) == 0
                                   select p).First();
                return product;
            }
            catch
            {
                return null;
            }
        }
    }
}
