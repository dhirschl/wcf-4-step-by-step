﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ShoppingCartService
{
    // Shopping cart item
    class ShoppingCartItem
    {
        public string ProductNumber { get; set; }
        public string ProductName { get; set; }
        public decimal Cost { get; set; }
        public int Volume { get; set; }
    }

    [ServiceContract(Namespace = "http://adventure-works.com/2010/06/04",
                     Name = "ShoppingCartService")]
    public interface IShoppingCartService
    {
        [OperationContract(Name = "AddItemToCart")]
        bool AddItemToCart(string productNumber);

        [OperationContract(Name = "RemoveItemFromCart")]
        bool RemoveItemFromCart(string productNumber);

        [OperationContract(Name = "GetShoppingCart")]
        string GetShoppingCart();

        [OperationContract(Name = "Checkout")]
        bool Checkout();
    } 
}
