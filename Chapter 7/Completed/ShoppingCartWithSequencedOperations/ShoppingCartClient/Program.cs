﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ShoppingCartClient.ShoppingCartService;

namespace ShoppingCartClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();
            try
            {
                // Connect to the ShoppingCartService service 
                ShoppingCartServiceClient proxy =
                    new ShoppingCartServiceClient("WS2007HttpBinding_IShoppingCartService");

                // Provide credentials to identify the user
                proxy.ClientCredentials.Windows.ClientCredential.Domain = "LON-DEV-01";
                proxy.ClientCredentials.Windows.ClientCredential.UserName = "Fred";
                proxy.ClientCredentials.Windows.ClientCredential.Password = "Pa$$w0rd";

                // Add two water bottles to the shopping cart 
                proxy.AddItemToCart("WB-H098");
                proxy.AddItemToCart("WB-H098");

                // Add a mountain seat assembly to the shopping cart 
                proxy.AddItemToCart("SA-M198");

                // Query the shopping cart and display the result 
                string cartContents = proxy.GetShoppingCart();
                Console.WriteLine(cartContents);

                // Buy the goods in the shopping cart 
                proxy.Checkout(); 
                Console.WriteLine("Goods purchased");

                // Go on another shopping expedition and buy more goods
                proxy = new ShoppingCartServiceClient("WS2007HttpBinding_IShoppingCartService");

                // Provide credentials to identify the user
                proxy.ClientCredentials.Windows.ClientCredential.Domain = "LON-DEV-01";
                proxy.ClientCredentials.Windows.ClientCredential.UserName = "Fred";
                proxy.ClientCredentials.Windows.ClientCredential.Password = "Pa$$w0rd";

                // Add a road seat assembly to the shopping cart 
                proxy.AddItemToCart("SA-R127");

                // Add a touring seat assembly to the shopping cart 
                proxy.AddItemToCart("SA-T872");

                // Remove the road seat assembly 
                proxy.RemoveItemFromCart("SA-R127");

                // Display the shopping basket 
                cartContents = proxy.GetShoppingCart();
                Console.WriteLine(cartContents);

                // Buy these goods as well 
                proxy.Checkout(); 
                Console.WriteLine("Goods purchased");  

                // Disconnect from the ShoppingCartService service 
                proxy.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
