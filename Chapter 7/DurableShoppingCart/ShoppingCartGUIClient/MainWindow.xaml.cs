﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ShoppingCartGUIClient.ShoppingCartService;

namespace ShoppingCartGUIClient
{
    public partial class MainWindow : Window
    {
        private ShoppingCartServiceClient proxy = null;
        private IDictionary<string, string> context = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Connect to the ShoppingCartService service 
            // proxy = new ShoppingCartServiceClient("WS2007HttpBinding_IShoppingCartService");
        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            // Disconnect from the service
            // proxy.Close();
        }

        // Add the item specified in the productNumber Text Box to the shopping cart
        private void addItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Create the proxy and connect to the service
                using (proxy = new ShoppingCartServiceClient("WSHttpContextBinding_IShoppingCartService"))
                {
                    // If the context is not null, then the client application
                    // has already created the durable session.
                    // Set the context in the IContextManager object for
                    // the proxy so that this context is passed in the SOAP
                    // header of the AddItemToCart and
                    // GetShoppingCart requests.
                    IContextManager contextManager = proxy.InnerChannel.GetProperty<IContextManager>();
                    if (context != null)
                    {
                        contextManager.SetContext(context);
                    }                    

                    // Add the item to the shoppping cart
                    proxy.AddItemToCart(productNumber.Text);

                    // If the context is null, then this was the first call
                    // made to the session.
                    // Capture the context and save it so that it can be
                    // passed to subsquent requests
                    if (context == null)
                    {
                        context = contextManager.GetContext();
                        MessageBox.Show(context["instanceId"], "New context created", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    
                    // Display the shopping cart
                    string cartContents = proxy.GetShoppingCart();
                    shoppingCartContents.Text = cartContents;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error adding item to cart", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        // Remove the item specified in the productNumber Text Box from the shopping cart
        private void removeItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                 // Create the proxy and connect to the service
                using (proxy = new ShoppingCartServiceClient("WSHttpContextBinding_IShoppingCartService"))
                {
                    // Set the context in the IContextManager object for the proxy so
                    // that this context is passed in the SOAP header of the
                    // RemoveItemFromCart and GetShoppingCart requests
                    IContextManager contextManager = proxy.InnerChannel.GetProperty<IContextManager>();
                    contextManager.SetContext(context);

                    // Remove the item from the shoppping cart
                    proxy.RemoveItemFromCart(productNumber.Text);

                    // Display the shopping cart
                    string cartContents = proxy.GetShoppingCart();
                    shoppingCartContents.Text = cartContents;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error removing item from cart", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // Checkout
        private void checkout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                   // Create the proxy and connect to the service
                using (proxy = new ShoppingCartServiceClient("WSHttpContextBinding_IShoppingCartService"))
                {
                    // Set the context in the IContextManager object for the proxy so
                    // that this context is passed in the SOAP header of the
                    // Checkout requests
                    IContextManager contextManager = proxy.InnerChannel.GetProperty<IContextManager>();
                    contextManager.SetContext(context);

                    proxy.Checkout();

                    // Clear the shopping cart displayed in the window
                    shoppingCartContents.Clear();

                    // Clear the context - the session has completed
                    context = null;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error checking out", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
