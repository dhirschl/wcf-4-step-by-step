﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProductsEntityModel;
using System.Xml.Serialization;
using System.IO;

namespace ShoppingCartService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class ShoppingCartServiceImpl : IShoppingCartService
    {
        private List<ShoppingCartItem> shoppingCart =
            new List<ShoppingCartItem>();

        // Examine the shopping cart to determine whether an item with a  
        // specified product number has already been added. 
        // If so, return a reference to the item, otherwise return null 
        private ShoppingCartItem find(List<ShoppingCartItem> shoppingCart,
                                      string productNumber)
        {
            foreach (ShoppingCartItem item in shoppingCart)
            {
                if (string.Compare(item.ProductNumber, productNumber) == 0)
                {
                    return item;
                }
            }

            return null;
        }

        public bool AddItemToCart(string productNumber)
        {
            // Note: For clarity, this method performs very limited
            // security checking and exception handling

            Console.WriteLine("AddItemToCart operation started");
            System.Threading.Thread.Sleep(10000);

            try
            {
                // Check to see whether the user has already added this
                // product to the shopping cart
                ShoppingCartItem item = find(shoppingCart, productNumber);

                // If so, then simply increment the volume
                if (item != null)
                {
                    item.Volume++;
                    Console.WriteLine("AddItemToCart operation completed");
                    return true;
                }

                // Otherwise, retrieve the details of the product from the database
                else
                {
                    // Connect to the AdventureWorks database by using the Entity Framework
                    using (AdventureWorksEntities database = new AdventureWorksEntities())
                    {
                        // Retrieve the details of the selected product
                        Product product = (from p in database.Products
                                           where string.Compare(p.ProductNumber, productNumber) == 0
                                           select p).First();
                        
                        // Create and populate a new shopping cart item
                        ShoppingCartItem newItem = new ShoppingCartItem
                        {
                            ProductNumber = product.ProductNumber,
                            ProductName = product.Name,
                            Cost = product.ListPrice,
                            Volume = 1
                        };

                        // Add the new item to the shopping cart
                        shoppingCart.Add(newItem);

                        // Indicate success
                        Console.WriteLine("AddItemToCart operation completed");
                        return true;
                    }
                }
            }
            catch
            {
                // If an error occurs, finish and indicate failure
                Console.WriteLine("AddItemToCart operation failed");
                return false;
            }
        }

        public bool RemoveItemFromCart(string productNumber)
        {
            // Determine whether the specified product has an  
            // item in the shopping cart

            Console.WriteLine("RemoveItemFromCart operation started");
            System.Threading.Thread.Sleep(10000);

            ShoppingCartItem item = find(shoppingCart, productNumber);

            // If so, then decrement the volume 
            if (item != null)
            {
                item.Volume--;

                // If the volume is zero, remove the item from the shopping cart 
                if (item.Volume == 0)
                {
                    shoppingCart.Remove(item);
                }

                // Indicate success
                Console.WriteLine("RemoveItemFromCart operation completed");
                return true;
            }

            // No such item in the shopping cart 
            Console.WriteLine("RemoveItemFromCart operation completed");
            return false;
        }

        public string GetShoppingCart()
        {
            // Create a string holding a formatted representation  
            // of the shopping cart

            Console.WriteLine("GetShoppingCart operation started");
            System.Threading.Thread.Sleep(10000);

            string formattedContent = String.Empty;
            decimal totalCost = 0;

            foreach (ShoppingCartItem item in shoppingCart)
            {
                string itemString = String.Format(
                       "Number: {0}\tName: {1}\tCost: {2:C}\tVolume: {3}",
                       item.ProductNumber, item.ProductName, item.Cost,
                       item.Volume);
                totalCost += (item.Cost * item.Volume);
                formattedContent += itemString + "\n";
            }

            string totalCostString = String.Format("\nTotalCost: {0:C}", totalCost);
            formattedContent += totalCostString;

            Console.WriteLine("GetShoppingCart operation completed");

            return formattedContent;
        }

        public bool Checkout()
        {
            Console.WriteLine("Checkout operation started");
            System.Threading.Thread.Sleep(10000);

            // Not currently implemented
            // - just return true

            Console.WriteLine("Checkout operation completed");
            return true;
        }
    }
}
