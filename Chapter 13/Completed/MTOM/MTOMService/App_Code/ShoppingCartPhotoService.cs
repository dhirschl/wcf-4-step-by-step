﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using ProductsPhotoModel;

namespace ShoppingCartPhotoService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class ShoppingCartPhotoServiceImpl : IShoppingCartPhotoService
    {
        public bool GetPhoto(string productNumber, out byte[] photo)
        {
            try
            {
                // Connect to the AdventureWorks database by using the Entity Framework
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Retrieve the photograph of the selected product
                    ProductPhoto photoData = (from p in database.Products                             
                                              where string.Compare(p.ProductNumber, productNumber) == 0
                                              join ph in database.ProductProductPhotos
                                              on p.ProductID equals ph.ProductID
                                              join phd in database.ProductPhotos
                                              on ph.ProductPhotoID equals phd.ProductPhotoID
                                              select phd).First();

                    // Copy the data for the photograph into the photo variable
                    photo = photoData.LargePhoto;

                    // Return true to indicate success
                    return true;
                }
            }
            catch (Exception e)
            {
                // If an exception occurs (possibly no such product) then
                // Set photo to null and return false
                photo = null;
                return false;
            }
        }
    }
}