﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Data;
using System.IO;
using System.Xml.Serialization;
using ProductsPhotoModel;

namespace ShoppingCartPhotoService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class ShoppingCartPhotoServiceImpl : IShoppingCartPhotoService
    {
        public Stream GetPhoto(string productNumber)
        {
            try
            {
                // Connect to the AdventureWorks database by using the Entity Framework
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Retrieve the photograph of the selected product
                    ProductPhoto photoData = (from p in database.Products                             
                                              where string.Compare(p.ProductNumber, productNumber) == 0
                                              join ph in database.ProductProductPhotos
                                              on p.ProductID equals ph.ProductID
                                              join phd in database.ProductPhotos
                                              on ph.ProductPhotoID equals phd.ProductPhotoID
                                              select phd).First();

                    // Return the photo as a stream
                    MemoryStream data = new MemoryStream(photoData.LargePhoto);
                    return data;
                }
            }
            catch (Exception e)
            {
                // If an exception occurs (possibly no such product) then return null
                return null;
            }
        }
    }
}