using System;
using System.ServiceModel;
using System.IO;

namespace ShoppingCartPhotoService
{
    [ServiceContract(Namespace = "http://adventure-works.com/2010/07/01", 
                     Name = "ShoppingCartPhotoService")]
    public interface IShoppingCartPhotoService
    {
        [OperationContract(Name = "GetPhoto")]
        Stream GetPhoto(string productNumber);
    }
}
