using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

using ShoppingCartGUIClient.ShoppingCartPhotoService;


namespace ShoppingCartGUIClient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>

    public partial class ClientWindow : System.Windows.Window
    {

        public ClientWindow()
        {
            InitializeComponent();
        }

        void getPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                // Connect to the ShoppingCartService service
                ShoppingCartPhotoServiceClient proxy =
                    new ShoppingCartPhotoServiceClient("BasicHttpBinding_ShoppingCartPhotoService");

                // Get the photo for the specified product
                string productNumber = this.productNumber.Text;
                Stream photo = proxy.GetPhoto(productNumber);
                if (photo != null)
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = photo;
                    bitmap.EndInit();
                    this.photoImage.Source = bitmap;
                }

                // Disconnect from the ShoppingCartService service
                proxy.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
            }
        }
    }
}