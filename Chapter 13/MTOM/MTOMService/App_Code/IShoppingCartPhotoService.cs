using System;
using System.ServiceModel;

namespace ShoppingCartPhotoService
{
    [ServiceContract(Namespace = "http://adventure-works.com/2010/07/01", 
                     Name = "ShoppingCartPhotoService")]
    public interface IShoppingCartPhotoService
    {
        [OperationContract(Name = "GetPhoto")]
        bool GetPhoto(string productNumber, out byte[] photo);
    }
}
