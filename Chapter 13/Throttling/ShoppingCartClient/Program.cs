﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using ShoppingCartClient.ShoppingCartService;


namespace ShoppingCartClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();

            Parallel.For(0, 10, (clientNum) => doClientWork(clientNum));

            Console.WriteLine("Tests complete. Press ENTER to finish");
            Console.ReadLine();
        }

        private static void doClientWork(int clientNum)
        {
            try
            {
                // Connect to the ShoppingCartService service
                ShoppingCartServiceClient proxy =
                    new ShoppingCartServiceClient("NetTcpBinding_IShoppingCartService");

                // Add two water bottles to the shopping cart
                Console.WriteLine("Client {0}: 1st AddItemToCart", clientNum);
                proxy.AddItemToCart("WB-H098");
                Console.WriteLine("Client {0}: 2nd AddItemToCart", clientNum);
                proxy.AddItemToCart("WB-H098");

                // Add a mountain seat assembly to the shopping cart
                Console.WriteLine("Client {0}: 3rd AddItemToCart", clientNum);
                proxy.AddItemToCart("SA-M198");

                // Query the shopping cart and display the result
                Console.WriteLine("Client {0}: GetShoppingCart", clientNum);
                string cartContents = proxy.GetShoppingCart();
                Console.WriteLine(cartContents);

                // Buy the goods in the shopping cart
                Console.WriteLine("Client {0}: Checkout", clientNum);
                if (proxy.Checkout())
                {
                    Console.WriteLine("Client {0}: Goods purchased", clientNum);
                }

                // Disconnect from the ShoppingCartService service
                proxy.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }
    }
}
