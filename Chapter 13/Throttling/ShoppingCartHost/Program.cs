﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Dispatcher;

namespace ShoppingCartHost
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost host = new ServiceHost(
            typeof(ShoppingCartService.ShoppingCartServiceImpl));
            host.Open();

            ChannelDispatcher dispatcher = (ChannelDispatcher)host.ChannelDispatchers[0];
            ServiceThrottle throttle = dispatcher.ServiceThrottle;
            if (throttle == null)
                Console.WriteLine("Service is using default throttling behavior");
            else
                Console.WriteLine("Instances: {0}\nCalls: {1}\nSessions: {2}", throttle.MaxConcurrentInstances, throttle.MaxConcurrentCalls, throttle.MaxConcurrentSessions);

            Console.WriteLine("Service running");
            Console.WriteLine("Press ENTER to stop the service");
            Console.ReadLine();
            host.Close();
        }
    }
}
