using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Configuration;
using ASPNETProductsClient.ProductsService;

namespace ASPNETProductsClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a proxy object and connect to the service
            PermissiveCertificatePolicy.Enact("CN=ASPNETProductsService");
            ProductsService.ProductsService proxy = new ProductsService.ProductsService();

            // Test the operations in the service
            try
            {
                // Obtain a list of all products
                Console.WriteLine("Test 1: List all products");
                string[] productNumbers = proxy.ListProducts();
                foreach (string productNumber in productNumbers)
                {
                    Console.WriteLine("Number: " + productNumber);
                }
                Console.WriteLine();

                // Fetch the details for a specific product
                Console.WriteLine("Test 2: Display the details of a product");
                Product product = proxy.GetProduct("WB-H098");
                Console.WriteLine("Number: " + product.ProductNumber);
                Console.WriteLine("Name: " + product.Name);
                Console.WriteLine("Color: " + product.Color);
                Console.WriteLine("Price: " + product.ListPrice);
                Console.WriteLine();

                // Query the stock level of this product
                Console.WriteLine("Test 3: Display the stock level of a product");
                int numInStock = proxy.CurrentStockLevel("WB-H098");
                Console.WriteLine("Current stock level: " + numInStock);
                Console.WriteLine();

                // Modify the stock level of this product
                Console.WriteLine("Test 4: Modify the stock level of a product");
                if (proxy.ChangeStockLevel("WB-H098", 100, "N/A", 0))
                {
                    numInStock = proxy.CurrentStockLevel("WB-H098");
                    Console.WriteLine("Stock changed. Current stock level: " + numInStock);
                }
                else
                {
                    Console.WriteLine("Stock level update failed");
                }
                Console.WriteLine();
            }

            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }

    // WARNING: This code is only needed for test certificates such as those created by makecert. It is 
    // not recommended for production code.
    class PermissiveCertificatePolicy
    {
        string subjectName;
        static PermissiveCertificatePolicy currentPolicy;
        PermissiveCertificatePolicy(string subjectName)
        {
            this.subjectName = subjectName;
            ServicePointManager.ServerCertificateValidationCallback +=
                new System.Net.Security.RemoteCertificateValidationCallback(RemoteCertValidate);
        }

        public static void Enact(string subjectName)
        {
            currentPolicy = new PermissiveCertificatePolicy(subjectName);
        }

        bool RemoteCertValidate(object sender, X509Certificate cert, X509Chain chain, System.Net.Security.SslPolicyErrors error)
        {
            if (cert.Subject == subjectName)
            {
                return true;
            }

            return false;
        }
    }
}
