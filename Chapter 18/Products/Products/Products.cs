using System;
using System.Collections;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.EnterpriseServices;
using System.Configuration;

namespace Products
{
    [Serializable]
    public class Product
    {
        public string Name;
        public string ProductNumber;
        public string Color;
        public decimal ListPrice;
    }

    [ComVisible(true)]
    [Guid("A04ED9CA-D61C-984B-AE4D-A164BDC90FD5")]
    public interface IProductsService
    {
        // Get the product number of all products
        ICollection ListProducts();

        // Get the details of a single product
        Product GetProduct(string productNumber);

        // Get the current stock level for a product
        int CurrentStockLevel(string productNumber);

        // Change the stock level for a product
        bool ChangeStockLevel(string productNumber, int newStockLevel, string shelf, int bin);
    }

    [ClassInterface(ClassInterfaceType.None)]
    [Transaction(TransactionOption.Supported)]
    public class ProductsService : ServicedComponent, IProductsService
    {
        private bool IsPotentialSqlInjectionAttack(string data)
        {
            // Check to see whether the data contains a rogue character
            // or the string "--", or the string "/*"
            char[] rogueChars = { ';', '\'', '\\', '"', '=', '%', '_', '*' };
            if ((data.IndexOfAny(rogueChars) != -1) ||
                 data.Contains("--") || data.Contains("/*"))
                return true;
            else
                return false;
        }

        public ICollection ListProducts()
        {
            SqlConnection advConn = null;

            try
            {
                // Connect to the AdventureWorks database
                string connectionString = ConfigurationManager.AppSettings["AdventureWorksConnection"];
                advConn = new SqlConnection(connectionString);
                advConn.Open();

                // Retrieve the details of all products by using a DataReader
                string queryString = "SELECT ProductNumber FROM Production.Product";

                SqlCommand advCmd = new SqlCommand(queryString, advConn);
                SqlDataReader productsReader = advCmd.ExecuteReader();

                // Create and populate a list of products
                ArrayList productsList = new ArrayList();
                while (productsReader.Read())
                {
                    string productNumber = productsReader.GetString(0);
                    productsList.Add(productNumber);
                }

                //Return the list of products
                return productsList;
            }

            finally
            {
                if (advConn != null)
                    advConn.Close();
            }
        }

        public Product GetProduct(string productNumber)
        {
            SqlConnection advConn = null;

            // Check for potential SQL Injection attack
            if (IsPotentialSqlInjectionAttack(productNumber))
            {
                return new Product();
            }

            try
            {
                // Connect to the AdventureWorks database
                string connectionString = ConfigurationManager.AppSettings["AdventureWorksConnection"];
                advConn = new SqlConnection(connectionString);
                advConn.Open();

                // Retrieve the details of the selected product by using a DataReader
                string queryString = @"SELECT ProductNumber, Name, Color, ListPrice 
                                       FROM Production.Product
                                       WHERE ProductNumber = '" + productNumber + "'";
                SqlCommand advCmd = new SqlCommand(queryString, advConn);
                SqlDataReader productsReader = advCmd.ExecuteReader();

                // Create and populate a product
                Product product = new Product();
                if (productsReader.Read())
                {
                    product.ProductNumber = productsReader.GetString(0);
                    product.Name = productsReader.GetString(1);
                    if (productsReader.IsDBNull(2))
                    {
                        product.Color = "N/A";
                    }
                    else
                    {
                        product.Color = productsReader.GetString(2);
                    }
                    product.ListPrice = productsReader.GetDecimal(3);
                }

                //Return the product
                return product;
            }

            finally
            {
                if (advConn != null)
                    advConn.Close();
            }
        }

        public int CurrentStockLevel(string productNumber)
        {
            SqlConnection advConn = null;

            // Check for potential SQL Injection attack
            if (IsPotentialSqlInjectionAttack(productNumber))
            {
                return 0;
            }

            // Connect to the AdventureWorks database
            string connectionString = ConfigurationManager.AppSettings["AdventureWorksConnection"];
            advConn = new SqlConnection(connectionString);
            advConn.Open();

            // Obtain the current stock level of the selected product
            // The stock level can be found by summing the quantity of the product available in all bins in the ProductInventory table
            // The ProductID value has to be retrieved from the Product table
            string queryString = @"SELECT SUM(Quantity) 
                                   FROM Production.ProductInventory 
                                   WHERE ProductID = 
                                      (SELECT ProductID 
                                       FROM Production.Product 
                                       WHERE ProductNumber = '" + productNumber + "')";

            SqlCommand advCmd = new SqlCommand(queryString, advConn);
            int stockLevel = (int)advCmd.ExecuteScalar();

            //Return the current stock level
            return stockLevel;
        }

        public bool ChangeStockLevel(string productNumber, int newStockLevel, string shelf, int bin)
        {
            SqlConnection advConn = null;

            // Check for potential SQL Injection attack
            if (IsPotentialSqlInjectionAttack(productNumber) || IsPotentialSqlInjectionAttack(shelf))
            {
                return false;
            }

            // Connect to the AdventureWorks database
            string connectionString = ConfigurationManager.AppSettings["AdventureWorksConnection"];
            advConn = new SqlConnection(connectionString);
            advConn.Open();

            // Modify the current stock level of the selected product
            // The ProductID value has to be retrieved from the Product table
            string updateString = @"UPDATE Production.ProductInventory 
                                    SET Quantity = Quantity + " + newStockLevel +
                                   "WHERE Shelf = '" + shelf + "'" +
                                   "AND Bin = " + bin +
                                  @"AND ProductID = 
                                        (SELECT ProductID 
                                         FROM Production.Product 
                                         WHERE ProductNumber = '" + productNumber + "')";
            SqlCommand advCmd = new SqlCommand(updateString, advConn);
            int numRowsChanged = (int)advCmd.ExecuteNonQuery();

            // If no rows were updated, return false to indicate that the input parameters 
            // did not identify a valid product and location.
            // Otherwise return true to indicate success
            return (numRowsChanged != 0);
        }
    }
}
