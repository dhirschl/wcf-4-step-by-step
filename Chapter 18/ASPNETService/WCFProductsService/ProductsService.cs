﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

// WCF Service that implements the service contract
// This implementation performs minimal error checking and exception handling
public class ProductsServiceImpl : IProductsService
{
    public ArrayOfString ListProducts()
    {
        // Create a list for holding product numbers
        ArrayOfString productsList = new ArrayOfString();

        try
        {
            // Connect to the AdventureWorks database by using the Entity Framework
            using (ProductsEntityModel.AdventureWorksEntities database = new ProductsEntityModel.AdventureWorksEntities())
            {
                // Fetch the product number of every product in the database
                var products = from product in database.Products
                               select product.ProductNumber;

                foreach (var product in products)
                {
                    productsList.Add(product);
                }
            }
        }
        catch
        {
            // Ignore exceptions in this implementation
        }

        // Return the list of product numbers
        return productsList;
    }

    public Product GetProduct(string productNumber)
    {
        // Create a reference to a ProductData object
        // This object will be instantiated if a matching product is found
        Product productData = null;

        try
        {
            // Connect to the AdventureWorks database by using the Entity Framework
            using (ProductsEntityModel.AdventureWorksEntities database = new ProductsEntityModel.AdventureWorksEntities())
            {
                // Find the first product that matches the specified product number
                ProductsEntityModel.Product matchingProduct =
                    database.Products.First(p => String.Compare(p.ProductNumber, productNumber) == 0);

                productData = new Product()
                {
                    Name = matchingProduct.Name,
                    ProductNumber = matchingProduct.ProductNumber,
                    Color = matchingProduct.Color,
                    ListPrice = matchingProduct.ListPrice
                };
            }
        }
        catch
        {
            // Ignore exceptions in this implementation
        }

        // Return the product
        return productData;
    }

    public int CurrentStockLevel(string productNumber)
    {
        // Obtain the total stock level for the specified product.
        // The stock level is calculated by summing the quantity of the product
        // available in all the bins in the ProductInventory table.

        // The Product and ProductInventory tables are joined over the 
        // ProductID column.

        int stockLevel = 0;

        try
        {
            // Connect to the AdventureWorks database by using the Entity Framework
            using (ProductsEntityModel.AdventureWorksEntities database = new ProductsEntityModel.AdventureWorksEntities())
            {
                // Calculate the sum of all quantities for the specified product
                stockLevel = (from pi in database.ProductInventories
                              join p in database.Products
                              on pi.ProductID equals p.ProductID
                              where String.Compare(p.ProductNumber, productNumber) == 0
                              select (int)pi.Quantity).Sum();
            }
        }
        catch
        {
            // Ignore exceptions in this implementation
        }

        // Return the stock level
        return stockLevel;
    }

    public bool ChangeStockLevel(string productNumber, int newStockLevel, string shelf, int bin)
    {
        // Modify the current stock level of the selected product in the ProductInventory table.
        // If the update is successful then return true, otherwise return false.

        // The Product and ProductInventory tables are joined over the 
        // ProductID column.

        try
        {
            // Connect to the AdventureWorks database by using the Entity Framework
            using (ProductsEntityModel.AdventureWorksEntities database = new ProductsEntityModel.AdventureWorksEntities())
            {
                // Find the ProductID for the specified product

                int productID = (from p in database.Products
                                 where String.Compare(p.ProductNumber, productNumber) == 0
                                 select p.ProductID).First();

                // Find the ProductInventory object that matches the parameters passed
                // in to the operation
                ProductsEntityModel.ProductInventory productInventory = database.ProductInventories.First(
                    pi => String.Compare(pi.Shelf, shelf) == 0 &&
                          pi.Bin == bin &&
                          pi.ProductID == productID);

                // Update the stock level for the ProductInventory object
                productInventory.Quantity += (short)newStockLevel;

                // Save the change back to the database
                database.SaveChanges();
            }
        }
        catch
        {
            // If an exception occurs, return false to indicate failure
            return false;
        }

        // Return true to indicate success
        return true;
    }
}
