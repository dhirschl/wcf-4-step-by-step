﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

[DataContract(Namespace="http://adventure-works.com/2005/01/01")]
public class Product
{
    [DataMember(Order=0, Name="Name")]
    public string Name;

    [DataMember(Order = 1, Name = "ProductName")]
    public string ProductNumber;

    [DataMember(Order = 2, Name = "Color")]
    public string Color;

    [DataMember(Order = 3, Name = "ListPrice")]
    public decimal ListPrice;
}

// Data contract for serializing a list of string
// using the same schema as the ASP.NET Web Service
[CollectionDataContract(Namespace = "http://adventure-works.com/2005/01/01")]
public class ArrayOfString : List<string>
{
}

[ServiceContract(Namespace="http://adventure-works.com/2005/01/01", Name="ProductsService", SessionMode= SessionMode.Allowed)]
public interface IProductsService
{
    // Get the product number of all products
    [OperationContract(Action = "http://adventure-works.com/2005/01/01/ListProducts", ReplyAction="http://adventure-works.com/2005/01/01/ListProductsResponse")]
    [TransactionFlow(TransactionFlowOption.Allowed)]
    ArrayOfString ListProducts();

    // Get the details of a single product
    [OperationContract(Action = "http://adventure-works.com/2005/01/01/GetProduct", ReplyAction = "http://adventure-works.com/2005/01/01/GetProductResponse")]
    [TransactionFlow(TransactionFlowOption.Allowed)]
    Product GetProduct(string productNumber);

    // Get the current stock level for a product
    [OperationContract(Action = "http://adventure-works.com/2005/01/01/CurrentStockLevel", ReplyAction = "http://adventure-works.com/2005/01/01/CurrentStockLevelResponse")]
    [TransactionFlow(TransactionFlowOption.Allowed)]
    int CurrentStockLevel(string productNumber);

    // Change the stock level for a product
    [OperationContract(Action = "http://adventure-works.com/2005/01/01/ChangeStockLevel", ReplyAction = "http://adventure-works.com/2005/01/01/CheckStockLevelResponse")]
    [TransactionFlow(TransactionFlowOption.Allowed)]
    bool ChangeStockLevel(string productNumber, int newStockLevel, string shelf, int bin);
}
