﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Security;


namespace Products
{

    // Classes for passing fault information back to client applications
    [DataContract]
    public class SystemFault
    {
        [DataMember]
        public string SystemOperation { get; set; }

        [DataMember]
        public string SystemReason { get; set; }

        [DataMember]
        public string SystemMessage { get; set; }
    }

    [DataContract]
    public class DatabaseFault
    {
        [DataMember]
        public string DbOperation { get; set; }

        [DataMember]
        public string DbReason { get; set; }

        [DataMember]
        public string DbMessage { get; set; }
    }

    // Data contract describing the details of a product passed to client applications
    [DataContract(Namespace = "http://adventure-works.com/datacontract/2010/06/01/Products")]
    public class ProductData
    {
        [DataMember(Order=0)]
        public string Name;

        [DataMember(Order = 1)]
        public string ProductNumber;

        [DataMember(Order = 2)]
        public string Color;

        [DataMember(Order = 3)]
        public decimal ListPrice;

        [DataMember(Order = 0)]
        public decimal StandardCost;

        [DataMember(Order = 4)]
        public bool FinishedGoodsFlag;
    }

    // Service contract describing the operations provided by the WCF service
    [ServiceContract(Namespace="http://adventure-works.com/2010/02/28", Name="IProductsService")]
    public interface IProductsService
    {
        // Get the product number of every product
        [FaultContract(typeof(SystemFault))]
        [FaultContract(typeof(DatabaseFault))]
        [OperationContract(ProtectionLevel=ProtectionLevel.EncryptAndSign)]
        List<string> ListProducts();

        // Get the details of a single product
        [OperationContract(ProtectionLevel = ProtectionLevel.EncryptAndSign)]
        ProductData GetProduct(string productNumber);

        // Get the current stock level for a product
        [OperationContract(ProtectionLevel = ProtectionLevel.Sign)]
        int CurrentStockLevel(string productNumber);

        // Change the stock level for a product
        [OperationContract(ProtectionLevel = ProtectionLevel.Sign)]
        bool ChangeStockLevel(string productNumber, short newStockLevel, string shelf, int bin);
    }

    // Version 2 of the servica contract
    [ServiceContract(Namespace = "http://adventure-works.com/2010/05/31", Name = "IProductsService")]
    public interface IProductsServiceV2
    {
        // Get the product number of matching products
        [FaultContract(typeof(SystemFault))]
        [FaultContract(typeof(DatabaseFault))]
        [OperationContract]
        List<string> ListMatchingProducts(string match);

        // Get the details of a single product
        [OperationContract]
        ProductData GetProduct(string productNumber);

        // Get the current stock level for a product
        [OperationContract]
        int CurrentStockLevel(string productNumber);

        // Change the stock level for a product
        [OperationContract]
        bool ChangeStockLevel(string productNumber, short newStockLevel, string shelf, int bin);

        // Update the details of the specified product in the database
        [OperationContract]
        void UpdateProductDetails(ProductData product);
    }
}