﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

[ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Multiple)]
public class AdventureWorksAdmin : IAdventureWorksAdmin
{
    private delegate void AsyncSleepCaller(int millisecondsTimeout);

    public void GenerateDailySalesReport(string id)
    {
        // Simulate generating the report
        // by sleeping for 1 minute and 10 seconds
        System.Threading.Thread.Sleep(70000);

        string msg = String.Format("Report {0} generated", id);
        System.Windows.MessageBox.Show(msg);
    }


    // CalculateTotalValueOfStock operation
    // Service can elect to perform the operation
    // synchronously or asynchronously
    public IAsyncResult BeginCalculateTotalValueOfStock(string id, AsyncCallback callback, object state)
    {
        AsyncResult<int> calcTotalValueResult;

        // Generate a random number
        // The value generated deterines the "complexity" of the operation
        Random generator = new Random();

        // If the random number is even, then the operation is simple
        // so perform it synchronously
        if ((generator.Next() % 2) == 0)
        {
            calcTotalValueResult = new AsyncResult<int>(true, state);
            System.Threading.Thread.Sleep(20000);
            System.Windows.MessageBox.Show("Synchronous sleep completed");
            calcTotalValueResult.Data = 5555555;
            calcTotalValueResult.Complete();
        }

        // Otherwise, the operation is complete so perform it asynchronously
        else
        {
            // Perform the operation asynchronously
            calcTotalValueResult = new AsyncResult<int>(false, state);
            AsyncSleepCaller asyncSleep = new AsyncSleepCaller(System.Threading.Thread.Sleep);
            IAsyncResult result = asyncSleep.BeginInvoke(30000, new AsyncCallback(EndAsyncSleep), calcTotalValueResult);
        }

        callback(calcTotalValueResult);
        System.Windows.MessageBox.Show("BeginCalculateTotalValueOfStock completed for " + id);
        return calcTotalValueResult;
    }

    public int EndCalculateTotalValueOfStock(IAsyncResult r)
    {
       // Wait until the AsyncResult object indicates the
       // operation is complete
        AsyncResult<int> result = r as AsyncResult<int>;
        if (!result.CompletedSynchronously)
        {
            System.Threading.WaitHandle waitHandle = result.AsyncWaitHandle;
            waitHandle.WaitOne();
        }

        // Return the calculated value in the Data field
        return result.Data;
    }

    private void EndAsyncSleep(IAsyncResult ar)
    {
        // This delegate indicates that the "complex" calculation
        // has finished
        AsyncResult<int> calcTotalValueResult = (AsyncResult<int>)ar.AsyncState;
        calcTotalValueResult.Data = 9999999;
        calcTotalValueResult.Complete();

        System.Windows.MessageBox.Show("Asynchronous sleep completed");
    }
}