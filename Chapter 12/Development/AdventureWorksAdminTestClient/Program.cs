﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AdventureWorksAdminTestClient.AdventureWorksAdmin;

namespace AdventureWorksAdminTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            AdministrativeServiceClient proxy = new AdministrativeServiceClient("WS2007HttpBinding_AdministrativeService");

            proxy.BeginCalculateTotalValueOfStock("First Calculation", CalculateTotalValueCallback, proxy);
            proxy.BeginCalculateTotalValueOfStock("Second Calculation", CalculateTotalValueCallback, proxy);
            proxy.BeginCalculateTotalValueOfStock("Third Calculation", CalculateTotalValueCallback, proxy);

            //int totalValue = proxy.CalculateTotalValueOfStock("First Calculation");
            //Console.WriteLine("Total value of stock is {0}", totalValue);

            //totalValue = proxy.CalculateTotalValueOfStock("Second Calculation");
            //Console.WriteLine("Total value of stock is {0}", totalValue);

            //totalValue = proxy.CalculateTotalValueOfStock("Third Calculation");
            //Console.WriteLine("Total value of stock is {0}", totalValue);

            //proxy.Close();

            //try
            //{
            //   AdministrativeServiceClient proxy = new AdministrativeServiceClient("WS2007HttpBinding_AdministrativeService");
            //
            //    Console.WriteLine("Requesting first report at {0}", DateTime.Now);
            //    proxy.GenerateDailySalesReport("First Report");
            //    Console.WriteLine("First report request completed at {0}", DateTime.Now);
            //    Console.WriteLine("Requesting second report at {0}", DateTime.Now);
            //    proxy.GenerateDailySalesReport("Second Report");
            //    Console.WriteLine("Second report request completedat {0}", DateTime.Now);

            //    proxy.Close();
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine("Exception: {0}", e.Message);
            //}
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }

        static void CalculateTotalValueCallback(IAsyncResult asyncResult)
        {
            int total = ((AdministrativeServiceClient)asyncResult.AsyncState).EndCalculateTotalValueOfStock(asyncResult);
            Console.WriteLine("Total value of stock is {0}", total);
        }
    }
}
