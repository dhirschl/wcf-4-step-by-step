// CalculateTotalValueOfStock operation
// Service can elect to perform the operation 
// synchronously or asynchronously
public IAsyncResult BeginCalculateTotalValueOfStock(string id,
    AsyncCallback callback, object state)
{
    AsyncResult<int> calcTotalValueResult;

    // Generate a random number. 
    // The value generated determines the "complexity" of the operation
    Random generator = new Random();

    // If the random number is even, then the operation is simple 
    // so perform it synchronously
    if ((generator.Next() % 2) == 0)
    {
        calcTotalValueResult = new AsyncResult<int>(true, state);
        System.Threading.Thread.Sleep(20000);
        System.Windows.MessageBox.Show("Synchronous sleep completed");
        calcTotalValueResult.Data = 5555555;
        calcTotalValueResult.Complete();
    }
    // Otherwise, the operation is complex so perform it asynchronously
    else
    {
        // Perform the operation asynchronously
        calcTotalValueResult = new AsyncResult<int>(false, state);
        AsyncSleepCaller asyncSleep = new AsyncSleepCaller(System.Threading.Thread.Sleep);
        IAsyncResult result = asyncSleep.BeginInvoke(30000,
            new AsyncCallback(EndAsyncSleep), calcTotalValueResult);
    }

    callback(calcTotalValueResult);
    System.Windows.MessageBox.Show("BeginCalculateTotalValueOfStock completed for " + id);
    return calcTotalValueResult;
}
