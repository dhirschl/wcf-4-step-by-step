﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

[ServiceContract(Namespace = "http://adventure-works.com/2010/06/30",
                 Name = "AdministrativeService")]
public interface IAdventureWorksAdmin
{
    [OperationContract(IsOneWay = true)]
    void GenerateDailySalesReport(string id);

    [OperationContract(AsyncPattern = true)]
    IAsyncResult BeginCalculateTotalValueOfStock(string id, AsyncCallback cb, object s);
    int EndCalculateTotalValueOfStock(IAsyncResult r);
}