﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdventureWorksAdminTestClient.AdventureWorksAdmin;

namespace AdventureWorksAdminTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                AdministrativeServiceClient proxy =
                    new AdministrativeServiceClient("WS2007HttpBinding_AdministrativeService");

                proxy.BeginCalculateTotalValueOfStock("First Calculation",
                    CalculateTotalValueCallback, proxy);
                proxy.BeginCalculateTotalValueOfStock("Second Calculation",
                    CalculateTotalValueCallback, proxy);
                proxy.BeginCalculateTotalValueOfStock("Third Calculation",
                    CalculateTotalValueCallback, proxy);

                //proxy.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }

        static void CalculateTotalValueCallback(IAsyncResult asyncResult)
        {
            int total = ((AdministrativeServiceClient)asyncResult.AsyncState).
                EndCalculateTotalValueOfStock(asyncResult);
            Console.WriteLine("Total value of stock is {0}", total);
        }
    }
}
