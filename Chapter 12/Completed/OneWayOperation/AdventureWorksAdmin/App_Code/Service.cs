﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
public class AdventureWorksAdmin : IAdventureWorksAdmin
{
    public void GenerateDailySalesReport(string id)
    {
        // Simulate generating the report
        // by sleeping for 1 minute and 10 seconds
        System.Threading.Thread.Sleep(70000);
        string msg = String.Format("Report {0} generated", id);
        System.Windows.MessageBox.Show(msg);
    }
}