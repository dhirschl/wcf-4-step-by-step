private void EndAsyncSleep(IAsyncResult ar)
{
    // This delegate indicates that the "complex" calculation 
    // has finished
    AsyncResult<int> calcTotalValueResult = 
        (AsyncResult<int>)ar.AsyncState;
    calcTotalValueResult.Data = 9999999;
    calcTotalValueResult.Complete();

    System.Windows.MessageBox.Show("Asynchronous sleep completed");
}
