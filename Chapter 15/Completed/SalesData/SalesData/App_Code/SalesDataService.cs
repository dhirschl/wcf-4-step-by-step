﻿using System;
using System.Data.Services;
using System.Data.Services.Common;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;
using AdventureWorksModel;

public class SalesDataService : DataService<AdventureWorksEntities>
{
    // This method is called only once to initialize service-wide policies.
    public static void InitializeService(DataServiceConfiguration config)
    {
        config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V2;
        config.SetEntitySetAccessRule("Contacts", EntitySetRights.AllRead);
        config.SetEntitySetAccessRule("SalesOrderHeaders", EntitySetRights.AllRead);
        config.SetEntitySetAccessRule("SalesOrderDetails", EntitySetRights.AllRead);
        config.SetEntitySetPageSize("*", 25);
    }
}
