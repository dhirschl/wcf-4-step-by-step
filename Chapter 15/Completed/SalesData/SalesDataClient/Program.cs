﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalesDataClient.SalesDataService;

namespace SalesDataClient
{
    class Program
    {
        static void Main(string[] args)
        {
            AdventureWorksEntities service = new AdventureWorksEntities(
                new Uri("http://localhost:48000/SalesData/SalesDataService.svc"));
            
            Console.WriteLine("Test 1: List details of contacts");
            foreach (Contact contact in service.Contacts)
            {
                Console.WriteLine("ID: {0}\nFirst Name: {1}\nLast Name: {2}\n", 
                    contact.ContactID, contact.FirstName, contact.LastName);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine("Test 2: List sales order details");
            foreach (SalesOrderDetail detail in service.SalesOrderDetails)
            {
                Console.WriteLine("Order ID: {0}\nProduct: {1}\nQuantity: {2}\nUnit Price: {3:C}\n", 
                    detail.SalesOrderID, detail.ProductID, detail.OrderQty, detail.UnitPrice);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine("Test 3: Skip the first 50 order details records");
            foreach (SalesOrderDetail detail in service.SalesOrderDetails.Skip(50))
            {
                Console.WriteLine("Order ID: {0}\nProduct: {1}\nQuantity: {2}\nUnit Price: {3:C}\n", 
                    detail.SalesOrderID, detail.ProductID, detail.OrderQty, detail.UnitPrice);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine("Test 4: Sort data by unit price");
            foreach (SalesOrderDetail detail in service.SalesOrderDetails.OrderBy((d) => d.UnitPrice))
            {
                Console.WriteLine("Order ID: {0}\nProduct: {1}\nQuantity: {2}\nUnit Price: {3:C}\n", 
                    detail.SalesOrderID, detail.ProductID, detail.OrderQty, detail.UnitPrice);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine("Test 5: Sort data by unit price (most expensive first)");
            foreach (SalesOrderDetail detail in 
                service.SalesOrderDetails.OrderByDescending((d) => d.UnitPrice))
            {
                Console.WriteLine("Order ID: {0}\nProduct: {1}\nQuantity: {2}\nUnit Price: {3:C}\n", 
                    detail.SalesOrderID, detail.ProductID, detail.OrderQty, detail.UnitPrice);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine(
                "Test 6: Display the SalesOrderID and TotalDue for all orders for customer 99");
            foreach (var orderData in 
                     from o in service.SalesOrderHeaders
                     where o.CustomerID == 99
                     select new {o.SalesOrderID, o.TotalDue})
            {
                Console.WriteLine("Order ID: {0}\nTotal Due: {1:C}\n", 
                    orderData.SalesOrderID, orderData.TotalDue);
            }
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            Console.WriteLine("Test 7: fetch the contact details for all orders");
            foreach (var orderData in
                     from o in service.SalesOrderHeaders.Expand("Contact")
                     select o)
            {
                Console.WriteLine("Order ID: {0}\nCustomer: {1} {2}\n", 
                    orderData.SalesOrderID, orderData.Contact.FirstName, orderData.Contact.LastName);
            }
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
