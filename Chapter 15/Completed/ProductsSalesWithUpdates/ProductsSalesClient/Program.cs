﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProductsSalesService;

namespace ProductsSalesClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();

            // Create a proxy object and connect to the service
            ProductsSalesProxy proxy = new ProductsSalesProxy();

            // Test the operations in the service

            try
            {
                // Obtain a list of 30 orders, starting with the 11th
                Console.WriteLine("Test 1: List orders");
                ICollection<SalesOrderHeader> orders = proxy.GetAllOrders(10, 30);
                Console.WriteLine("Order\tDate Placed\tCustomer\tAmount Due");
                foreach (SalesOrderHeader order in orders)
                {
                    Console.WriteLine("{0}\t{1:d}\t{2}\t\t{3:C}",
                        order.SalesOrderID, order.OrderDate, order.CustomerID, order.TotalDue);
                }
                Console.WriteLine();

                // Find the details for order 43687
                Console.WriteLine("Test 2: Get details for order 43687");
                SalesOrderHeader salesOrder = proxy.GetOrder("43687");
                Console.WriteLine("Order ID: {0}\nDate Placed {1}\nCustomer ID: {2}\nAmount Due: {3:C}\n\n",
                        salesOrder.SalesOrderID, salesOrder.OrderDate, salesOrder.CustomerID, salesOrder.TotalDue);

                // Find the customer that placed order 43687
                Console.WriteLine("Test 3: Find the customer for order 43687");
                Contact salesCustomer = proxy.GetCustomerForOrder("43687");
                Console.WriteLine("Customer: {0} {1}\nEmail: {2}\nPhone: {3}\n\n",
                        salesCustomer.FirstName, salesCustomer.LastName, salesCustomer.EmailAddress, salesCustomer.Phone);

                // Find all customers with an ID in the range 75 to 90
                Console.WriteLine("Test 4: List customers");
                ICollection<Contact> customers = proxy.GetAllCustomers(74, 15);
                Console.WriteLine("Name\t\tEmail\t\t\t\tPhone");
                foreach (Contact customer in customers)
                {
                    Console.WriteLine("{0} {1}\t{2}\t{3}",
                            customer.FirstName, customer.LastName, customer.EmailAddress, customer.Phone);
                }
                Console.WriteLine();

                // Find the details of customer 99
                Console.WriteLine("Test 5: Find the details for customer 99");
                salesCustomer = proxy.GetCustomer("99");
                Console.WriteLine("Customer: {0} {1}\nEmail: {2}\nPhone: {3}\n\n",
                        salesCustomer.FirstName, salesCustomer.LastName, salesCustomer.EmailAddress, salesCustomer.Phone);

                // Find all orders placed by customer 99
                Console.WriteLine("Test 6: Find all orders for customer 99");
                orders = proxy.GetOrdersForCustomer("99");
                Console.WriteLine("Order\tDate Placed\tCustomer\tAmount Due");
                foreach (SalesOrderHeader order in orders)
                {
                    Console.WriteLine("{0}\t{1:d}\t{2}\t\t{3:C}",
                        order.SalesOrderID, order.OrderDate, order.CustomerID, order.TotalDue);
                }
                Console.WriteLine();

                // Create a new customer
                Console.WriteLine("Test 7: Create a new customer");
                int customerID = proxy.CreateCustomer("John", "Sharp", "john@adventure-works.com", "(123)456789");
                salesCustomer = proxy.GetCustomer(customerID.ToString());
                Console.WriteLine("Customer ID: {0}\nName: {1} {2}\nEmail: {3}\nPhone: {4}\n\n",
                        salesCustomer.ContactID, salesCustomer.FirstName, salesCustomer.LastName, salesCustomer.EmailAddress, salesCustomer.Phone);

                // Change the email address for the new customer
                Console.WriteLine("Test 8: Change the email address for the new customer");
                proxy.UpdateCustomer(customerID.ToString(), email: "newaddress@adventure-works.com");
                salesCustomer = proxy.GetCustomer(customerID.ToString());
                Console.WriteLine("Customer ID: {0}\nName: {1} {2}\nEmail: {3}\nPhone: {4}\n\n",
                        salesCustomer.ContactID, salesCustomer.FirstName, salesCustomer.LastName, salesCustomer.EmailAddress, salesCustomer.Phone);

                // Delete the new customer
                Console.WriteLine("Test 9: Delete a customer");
                proxy.DeleteCustomer(customerID.ToString());
                salesCustomer = proxy.GetCustomer(customerID.ToString());
                if (salesCustomer == null)
                    Console.WriteLine("Customer deleted");
                else
                    Console.WriteLine("Customer not deleted");

                // Disconnect from the service
                proxy.Close();
            }

            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    Console.WriteLine("{0}", e.InnerException.Message);
                }
                else
                {
                    Console.WriteLine("General exception: {0}", e.Message);
                }
            }

            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
