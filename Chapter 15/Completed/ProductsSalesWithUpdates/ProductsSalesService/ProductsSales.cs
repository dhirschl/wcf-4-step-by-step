﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;

namespace ProductsSalesService
{
    public class ProductsSales : IProductsSales
    {
        // Return a list of orders
        public ICollection<SalesOrderHeader> GetAllOrders(int skip, int top)
        {
            List<SalesOrderHeader> salesOrders = null;

            if (top == 0)
            {
                top = 100;
            }

            try
            {
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    salesOrders = (from order in database.SalesOrderHeaders
                                   orderby order.SalesOrderID
                                   select order).Skip(skip).Take(top).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return salesOrders;
        }

        // Return the details of the specified order
        public SalesOrderHeader GetOrder(string orderID)
        {
            SalesOrderHeader header = null;

            try
            {
                int id = Convert.ToInt32(orderID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    header = (from order in database.SalesOrderHeaders
                              where order.SalesOrderID == id
                              select order).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return header;
        }

        // Return the details of the customer for the specified order
        public Contact GetCustomerForOrder(string orderID)
        {
            Contact orderCustomer = null;

            try
            {
                int id = Convert.ToInt32(orderID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomer = (from customer in database.Contacts
                                     join order in database.SalesOrderHeaders
                                     on customer.ContactID equals order.CustomerID
                                     where order.SalesOrderID == id
                                     select customer).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomer;
        }

        // Return a list of customers
        public ICollection<Contact> GetAllCustomers(int skip, int top)
        {
            List<Contact> orderCustomers = null;

            if (top == 0)
            {
                top = 100;
            }

            try
            {
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomers = (from customer in database.Contacts
                                      orderby customer.ContactID
                                      select customer).Skip(skip).Take(top).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomers;
        }

        // Return the details of the specified customer
        public Contact GetCustomer(string customerID)
        {
            Contact orderCustomer = null;

            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomer = (from customer in database.Contacts
                                     where customer.ContactID == id
                                     select customer).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomer;
        }

        // Return the orders for the specified customer
        public ICollection<SalesOrderHeader> GetOrdersForCustomer(string customerID)
        {
            List<SalesOrderHeader> salesOrders = null;

            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    salesOrders = (from customer in database.Contacts
                                   join order in database.SalesOrderHeaders
                                   on customer.ContactID equals order.CustomerID
                                   where customer.ContactID == id
                                   select order).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return salesOrders;
        }

        // Create a new customer, and return the customer ID
        public int CreateCustomer(string firstName, string lastName, string email, string phone)
        {
            try
            {
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Create and populate a new Contact object
                    Contact newCustomer = new Contact()
                    {
                        FirstName = firstName,
                        LastName = lastName,
                        EmailAddress = email,
                        Phone = phone,
                        PasswordHash = "",
                        PasswordSalt = "",
                        rowguid = Guid.NewGuid(),
                        ModifiedDate = DateTime.Now
                    };

                    // Add the new customer to the database and save the changes
                    database.AddToContacts(newCustomer);
                    database.SaveChanges();
                    return newCustomer.ContactID;
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        // Update the email address and/or telephone number
        // for the specified customer
        public void UpdateCustomer(string customerID, string email, string phone)
        {
            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Find the customer in the database
                    var findCustomer = from customer in database.Contacts
                                       where customer.ContactID == id
                                       select customer;

                    if (findCustomer.Count() > 0)
                    {
                        // Update the details for the customer and save the changes
                        Contact customer = findCustomer.First();

                        if (email != null)
                            customer.EmailAddress = email;
                        if (phone != null)
                            customer.Phone = phone;

                        customer.ModifiedDate = DateTime.Now;
                        database.SaveChanges();
                    }
                    else
                    {
                        throw new WebFaultException(HttpStatusCode.NotFound);
                    }
                }
            }
            catch (Exception e)
            {
                if (e is WebFaultException)
                {
                    throw;
                }
                else
                {
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }

        // Delete the specified customer
        public void DeleteCustomer(string customerID)
        {
            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    // Find the customer in the database
                    var findCustomer = from customer in database.Contacts
                                       where customer.ContactID == id
                                       select customer;

                    if (findCustomer.Count() > 0)
                    {
                        // Remove the customer and save the changes
                        Contact customer = findCustomer.First();
                        database.DeleteObject(customer);
                        database.SaveChanges();
                    }
                    else
                    {
                        throw new WebFaultException(HttpStatusCode.NotFound);
                    }
                }
            }
            catch (Exception e)
            {
                if (e is WebFaultException)
                {
                    throw;
                }
                else
                {
                    throw new WebFaultException(HttpStatusCode.BadRequest);
                }
            }
        }
    }
}
