﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ProductsSalesService;

namespace ProductsSalesClient
{
    class ProductsSalesProxy : ClientBase<IProductsSales>, IProductsSales
    {
        public ICollection<SalesOrderHeader> GetAllOrders(int skip = 0, int top = 0)
        {
            return this.Channel.GetAllOrders(skip, top);
        }

        public SalesOrderHeader GetOrder(string orderID)
        {
            return this.Channel.GetOrder(orderID);
        }

        public Contact GetCustomerForOrder(string orderID)
        {
            return this.Channel.GetCustomerForOrder(orderID);
        }

        public ICollection<Contact> GetAllCustomers(int skip = 0, int top = 0)
        {
            return this.Channel.GetAllCustomers(skip, top);
        }

        public Contact GetCustomer(string customerID)
        {
            return this.Channel.GetCustomer(customerID);
        }

        public ICollection<SalesOrderHeader> GetOrdersForCustomer(string customerID)
        {
            return this.Channel.GetOrdersForCustomer(customerID);
        }
    }
}
