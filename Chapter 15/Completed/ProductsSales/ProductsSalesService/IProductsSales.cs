﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;

namespace ProductsSalesService
{
    [ServiceContract(Namespace = "http://adventure-works.com/2010/07/28",
                     Name = "ProductsSales")] 
    public interface IProductsSales
    {
        [OperationContract]
        [WebGet(UriTemplate = "Orders?skip={skip}&top={top}")]
        [Description("Returns a list of all orders. By default, the list is limited to the first 100 orders; specify the SKIP and TOP parameters to implement paging.")]
        ICollection<SalesOrderHeader> GetAllOrders(int skip, int top);

        [OperationContract]
        [WebGet(UriTemplate = "Orders/{orderID}")]
        [Description("Returns the details of an order")]
        SalesOrderHeader GetOrder(string orderID);

        [OperationContract]
        [WebGet(UriTemplate = "Orders/{orderID}/Customer")]
        [Description("Returns the details of the customer that placed the order")]
        Contact GetCustomerForOrder(string orderID);

        [OperationContract]
        [WebGet(UriTemplate = "Customers?skip={skip}&top={top}")]
        [Description("Returns a list of all customers")]
        ICollection<Contact> GetAllCustomers(int skip, int top);

        [OperationContract]
        [WebGet(UriTemplate = "Customers/{customerID}")]
        [Description("Returns the details of a customer")]
        Contact GetCustomer(string customerID);

        [OperationContract]
        [WebGet(UriTemplate = "Customers/{customerID}/Orders")]
        [Description("Returns the orders placed by a customer")]
        ICollection<SalesOrderHeader> GetOrdersForCustomer(string customerID);
    }
}
