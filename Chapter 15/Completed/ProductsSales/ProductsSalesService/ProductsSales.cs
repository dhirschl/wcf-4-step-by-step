﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Net;

namespace ProductsSalesService
{
    public class ProductsSales : IProductsSales
    {
        // Return a list of orders
        public ICollection<SalesOrderHeader> GetAllOrders(int skip, int top)
        {
            List<SalesOrderHeader> salesOrders = null;

            if (top == 0)
            {
                top = 100;
            }

            try
            {
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    salesOrders = (from order in database.SalesOrderHeaders
                                   orderby order.SalesOrderID
                                   select order).Skip(skip).Take(top).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return salesOrders;
        }

        // Return the details of the specified order
        public SalesOrderHeader GetOrder(string orderID)
        {
            SalesOrderHeader header = null;

            try
            {
                int id = Convert.ToInt32(orderID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    header = (from order in database.SalesOrderHeaders
                              where order.SalesOrderID == id
                              select order).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return header;
        }

        // Return the details of the customer for the specified order
        public Contact GetCustomerForOrder(string orderID)
        {
            Contact orderCustomer = null;

            try
            {
                int id = Convert.ToInt32(orderID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomer = (from customer in database.Contacts
                                     join order in database.SalesOrderHeaders
                                     on customer.ContactID equals order.CustomerID
                                     where order.SalesOrderID == id
                                     select customer).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomer;
        }

        // Return a list of customers
        public ICollection<Contact> GetAllCustomers(int skip, int top)
        {
            List<Contact> orderCustomers = null;

            if (top == 0)
            {
                top = 100;
            }

            try
            {
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomers = (from customer in database.Contacts
                                      orderby customer.ContactID
                                      select customer).Skip(skip).Take(top).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomers;
        }

        // Return the details of the specified customer
        public Contact GetCustomer(string customerID)
        {
            Contact orderCustomer = null;

            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    orderCustomer = (from customer in database.Contacts
                                     where customer.ContactID == id
                                     select customer).FirstOrDefault();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return orderCustomer;
        }

        // Return the orders for the specified customer
        public ICollection<SalesOrderHeader> GetOrdersForCustomer(string customerID)
        {
            List<SalesOrderHeader> salesOrders = null;

            try
            {
                int id = Convert.ToInt32(customerID);
                using (AdventureWorksEntities database = new AdventureWorksEntities())
                {
                    salesOrders = (from customer in database.Contacts
                                   join order in database.SalesOrderHeaders
                                   on customer.ContactID equals order.CustomerID
                                   where customer.ContactID == id
                                   select order).ToList();
                }
            }
            catch
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }

            return salesOrders;
        }
    }
}
