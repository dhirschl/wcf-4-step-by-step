﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.Runtime.Serialization;
using System.ServiceModel;
using ProductsEntityModel;

namespace ProductsWorkflowService
{
    public sealed class ChangeStockLevel : CodeActivity<bool>
    {
        public InArgument<AdventureWorksEntities> Database { get; set; }
        public InArgument<string> ProductNumber { get; set; }
        public InArgument<short> NewStockLevel { get; set; }
        public InArgument<string> Shelf { get; set; }
        public InArgument<int> Bin { get; set; }

        protected override bool Execute(CodeActivityContext context)
        {
            // Modify the current stock level of the selected product 
            // in the ProductInventory table.
            // If the update is successful then return true, otherwise return false.

            // The Product and ProductInventory tables are joined over the 
            // ProductID column.

            // Retrieve the data from the input arguments
            string productNumber = ProductNumber.Get(context);
            AdventureWorksEntities database = Database.Get(context);
            short newStockLevel = NewStockLevel.Get(context);
            string shelf = Shelf.Get(context);
            int bin = Bin.Get(context);

            Console.WriteLine();
            Console.WriteLine("Isolation Level: {0}", System.Transactions.Transaction.Current.IsolationLevel);
            Console.WriteLine("Transaction Identifier: {0}", System.Transactions.Transaction.Current.TransactionInformation.LocalIdentifier);
            Console.WriteLine("Transaction Status: {0}", System.Transactions.Transaction.Current.TransactionInformation.Status);

            try
            {
                // Find the ProductID for the specified product
                int productID =
                    (from p in database.Products
                     where String.Compare(p.ProductNumber, productNumber) == 0
                     select p.ProductID).First();

                // Find the ProductInventory object that matches the parameters passed
                // in to the operation
                ProductInventory productInventory = database.ProductInventories.First(
                    pi => String.Compare(pi.Shelf, shelf) == 0 &&
                          pi.Bin == bin &&
                          pi.ProductID == productID);

                // Update the stock level for the ProductInventory object
                productInventory.Quantity += newStockLevel;

                // Save the change back to the database
                database.SaveChanges();
            }
            catch
            {
                // If an exception occurs, return false to indicate failure
                return false;
            }

            // Return true to indicate success
            return true;
        }
    }
}
