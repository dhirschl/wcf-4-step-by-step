﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Statements;

namespace ProductsWorkflowClient
{

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press ENTER when the service has started");
            Console.ReadLine();
            WorkflowInvoker.Invoke(new ProductsClient());
            Console.WriteLine("Press ENTER to finish");
            Console.ReadLine();
        }
    }
}
