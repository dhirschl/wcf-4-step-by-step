﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProductsWorkflowClient.ProductsService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://adventure-works.com/", ConfigurationName="ProductsService.IProductsService")]
    public interface IProductsService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://adventure-works.com/IProductsService/ChangeStockLevel", ReplyAction="http://adventure-works.com/IProductsService/ChangeStockLevelResponse")]
        ProductsWorkflowClient.ProductsService.ChangeStockLevelResponse ChangeStockLevel(ProductsWorkflowClient.ProductsService.ChangeStockLevelRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ChangeStockLevel", WrapperNamespace="http://adventure-works.com/", IsWrapped=true)]
    public partial class ChangeStockLevelRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://adventure-works.com/", Order=0)]
        public string ProductNumber;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://adventure-works.com/", Order=1)]
        public short NewStockLevel;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://adventure-works.com/", Order=2)]
        public string Shelf;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://adventure-works.com/", Order=3)]
        public int Bin;
        
        public ChangeStockLevelRequest() {
        }
        
        public ChangeStockLevelRequest(string ProductNumber, short NewStockLevel, string Shelf, int Bin) {
            this.ProductNumber = ProductNumber;
            this.NewStockLevel = NewStockLevel;
            this.Shelf = Shelf;
            this.Bin = Bin;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="ChangeStockLevelResponse", WrapperNamespace="http://adventure-works.com/", IsWrapped=true)]
    public partial class ChangeStockLevelResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://adventure-works.com/", Order=0)]
        public bool Result;
        
        public ChangeStockLevelResponse() {
        }
        
        public ChangeStockLevelResponse(bool Result) {
            this.Result = Result;
        }
    }
}
