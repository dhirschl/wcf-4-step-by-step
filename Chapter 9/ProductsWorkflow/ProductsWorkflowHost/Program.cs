﻿using System;
using System.Linq;
using System.Activities;
using System.Activities.Statements;
using System.ServiceModel.Activities;
using System.Xaml;

namespace ProductsWorkflowHost
{

    class Program
    {
        static void Main(string[] args)
        {
            WorkflowService service = 
                XamlServices.Load(@"..\..\..\ProductsWorkflowService\ProductsService.xamlx") 
                as WorkflowService;

            WorkflowServiceHost host = new WorkflowServiceHost(service);
            host.Open();
            Console.WriteLine("Service running. Press ENTER to stop");
            Console.ReadLine();
            host.Close();
        }
    }
}
