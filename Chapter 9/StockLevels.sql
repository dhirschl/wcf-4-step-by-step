SELECT 'Water bottles in stock ', SUM(Quantity) 
FROM Production.ProductInventory
WHERE ProductID = 870; -- WB-H098

SELECT 'Mountain seat assemblies in stock', SUM (Quantity) 
FROM Production.ProductInventory
WHERE ProductID = 514; -- SA-M198
 